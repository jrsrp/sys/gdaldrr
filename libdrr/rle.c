/* A set of generic routines for compressing/uncompressing data by
   run-length encoding. 
   These routines are used by the drr format routines. 

   Neil Flood. 25 August 1992.
*/
#include <memory.h>

#include "rle.h"

/* The maximum which can be held by an unsigned char. */
#define MAX_UBYTE_VAL 255
/* The flag values for a compresses/not-compressed chunk of data. */
#define FLAG_COMPRESSED 1
#define FLAG_NOTCOMPRESSED 0

typedef unsigned char countType;

void drrCompress(data, nvals, compressedData, elementSize, compressedSize)
char *data, *compressedData;
int nvals, elementSize, *compressedSize;
{
    int i, countSize, fromNdx, toNdx, tooBig;
    countType *count;
    char *p1, *p2;

    countSize = sizeof(countType);

    /* Indices to the char arrays data and compressedData. */
    fromNdx = 0;
    toNdx = 1;

    /* Set the compression flag to 'compressed'. */
    compressedData[0] = FLAG_COMPRESSED;

    /* The pointer to the current repeat count. */
    count = (countType *)(&compressedData[toNdx]);

    /* Put in the first value, with a repeat count of 1. */
    (*count) = 1;
    p1 = &(compressedData[toNdx]) + countSize;
    p2 = &(data[fromNdx]);
    memcpy(p1, p2, elementSize);
    fromNdx += elementSize;

    tooBig = (toNdx >= ((nvals-1)*elementSize+1-countSize));
    /* Loop over the rest of the data array. */
    for (i=1; ((i<nvals) && (!tooBig)); i++) {
        p1 = &(compressedData[toNdx]) + countSize;
        p2 = &(data[fromNdx]);
        if ((memcmp(p1, p2, elementSize) == 0) && ((*count) < MAX_UBYTE_VAL)) {
            (*count)++;
        } else {
            /* Check if adding on the next element+count will make the
               whole thing bigger than the uncompressed data (plus flag). */
            tooBig = ((toNdx+2*(elementSize+countSize)) > (nvals*elementSize+1));
            if (!tooBig) {
                toNdx += (elementSize + countSize);
                count = (countType *)(&compressedData[toNdx]);
                p1 = &(compressedData[toNdx]) + countSize;
                (*count) = 1;
                memcpy(p1, p2, elementSize);
            }
        }
        fromNdx += elementSize;
    }

    /* The total size of the compressed data, in bytes. */
    *compressedSize = toNdx + countSize + elementSize;

    /* If we ended up taking up more room compressed than uncompressed then
       just copy over the data straight. */
    if (tooBig) {
        compressedData[0] = FLAG_NOTCOMPRESSED;
        for (i=0; i<nvals; i++) {
            p1 = &(data[i*elementSize]);
            p2 = &(compressedData[i*elementSize+1]);
            memcpy(p2, p1, elementSize);
        }
        *compressedSize = nvals*elementSize + 1;
    }
}


/* Routine to uncompress data compressed by the routine above. */
void drrUncompress(data, nvals, compressedData, elementSize, compressedSize)
char *compressedData, *data;
int elementSize, nvals, *compressedSize;
{
    int i, j, count, countSize, nv,  stepSize;
    char *p2;
    register char *p1, srcB1, srcB2, srcB3, srcB4;

    if (compressedData[0] == FLAG_NOTCOMPRESSED) {
        memcpy(data, (compressedData+1), (nvals*elementSize));
        i = nvals*elementSize + 1;
    } else {
        countSize = sizeof(countType);
        stepSize = countSize+elementSize;
        nv = 0;
        i = 1;
        p1 = data;
        p2 = compressedData + 1 + countSize;

        /* This switch is here so that we can hardcode the correct copy
           statements for the different element sizes. Earlier I had set this
           up as one block of code, using memcpy(,,elementSize), but this
           involves a function call on every element, making it very slow.
           The following code seems the best compromise between code clarity
           and speed of execution.
        */
        switch (elementSize) {
            case 1:
                while (nv < nvals) {
                    count = (int)(*((countType *)(compressedData+i)));
                    srcB1 = *p2;
                    for (j=0; j<count; j++) {
                        *p1 = srcB1;
                        p1++;
                    }

                    p2 += stepSize;
                    nv += count;
                    i += stepSize;
                }
                break;
            case 2:
                while (nv < nvals) {
                    count = (int)(*((countType *)(compressedData+i)));
                    srcB1 = *p2;
                    srcB2 = *(p2+1);
                    for (j=0; j<count; j++) {
                        *p1 = srcB1;
                        *(p1+1) = srcB2;
                        p1 += elementSize;
                    }

                    p2 += stepSize;
                    nv += count;
                    i += stepSize;
                }
                break;
            case 4:
                while (nv < nvals) {
                    count = (int)(*((countType *)(compressedData+i)));
                    srcB1 = *p2;
                    srcB2 = *(p2+1);
                    srcB3 = *(p2+2);
                    srcB4 = *(p2+3);
                    for (j=0; j<count; j++) {
                        *p1 = srcB1;
                        *(p1+1) = srcB2;
                        *(p1+2) = srcB3;
                        *(p1+3) = srcB4;
                        p1 += elementSize;
                    }

                    p2 += stepSize;
                    nv += count;
                    i += stepSize;
                }
                break;
            default:  /* Should never be used, as all elementSizes are
                         one of the above. */
                while (nv < nvals) {
                    count = (int)(*((countType *)(compressedData+i)));
                    for (j=0; j<count; j++) {
                        memcpy(p1, p2, elementSize);
                        p1 += elementSize;
                    }

                    p2 += stepSize;
                    nv += count;
                    i += stepSize;
                }
                break;
        }
    }

    *compressedSize = i;
}
