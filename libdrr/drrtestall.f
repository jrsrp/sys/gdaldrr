! Intended to be a comprehensive test of DRR library functionality.
! Tests everything I can think of, by creating datasets, writing
! them into DRR files, and reading them out again. 
!
! Written in f77, as a sort of lowest common denominator. 
!
! Contains a test against a file called stdhdr.drr. This is just a drr 
! file with the same header as all the tests write, but assumed to be written
! elsewhere (and therefore be correct :). If not found, it just skips that 
! test.
!
        program testall

        use drr

!        include 'drrf.h'
!        record /DRRhdr/ hdr

        type (DRRhdr) hdr
 
        print*,"===================================================="
        print*,"NOTE: when porting between 32 and 64 bit platforms, "
        print*,"      change: 1. C_PTR_KIND in drrf90.f90           "
        print*,"              2. character padding(3) in drrf90.f90 "
        print*,"===================================================="
        
        call makeHdr(hdr)
        call testStdHdr(hdr)
        call testHdr(hdr)
        call testInt1(hdr)
        call testInt2(hdr)
        call testInt4(hdr)
        call testReal(hdr)
        call testBlock(hdr)
        call testLine(hdr)
        call testNames(hdr)
        
        stop
!        end

        contains
        
        
        subroutine makeHdr(hdr)
        type (DRRhdr) hdr
!        include 'drrf.h'
!        record /DRRhdr/ hdr
        integer i
        
!        call report('START: makeHdr')
        call report('Generating hdr')
        hdr%vmajor = 1
        hdr%vminor = 0
        hdr%nrows = 80
        hdr%ncols = 40
        hdr%datatype = DRR_UBYTE
        hdr%projection = LAT_LONG_PROJ
        hdr%bottom = -44.0
        hdr%top = -10.0
        hdr%left = 112.0
        hdr%right = 154.0
        hdr%Nrasters = 0
        hdr%maxNrasters = 500
        hdr%linesPerTile = 20
        hdr%rastersNamed = 0
        hdr%textLine(1) = 'first line'
        hdr%textLine(2) = 'second line'
        hdr%textLine(3) = 'third line'
        do i=1, MAXIMUM_ALLOWED_RASTERS
            hdr%imgOffsets(i) = 0
        enddo
!        call report('END: makeHdr')
        return
        end subroutine makeHdr
        
        
        subroutine testStdHdr(hdr)
        type (DRRhdr) hdr, hdr2
!        include 'drrf.h'
!        record /DRRhdr/ hdr, hdr2
        integer MAXROWS, MAXCOLS
        parameter (MAXROWS = 100, MAXCOLS = 50)
        integer rast(MAXCOLS, MAXROWS)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs
        
!        call report('START: testStdHdr')
        call report('Testing std hdr against generated hdr')
        
        f = DRRopen('stdhdr.drr', 'r')
        if (f .eq. 0) then
            call report('Unable to find stdhdr.drr, skipping test')
        else
            rs = DRRrdHead(f, hdr2)
            call DRRclose(f)
            
            call compareHdrs(hdr, hdr2)
            
            if (hdr%textLine(1)(:10) .ne. 'first line') then
                call report('hdr.textLine(1) incorrect')
            endif
            if (hdr%textLine(2)(:11) .ne. 'second line') then
                call report('hdr.textLine(2) incorrect')
            endif
            if (hdr%textLine(3)(:10) .ne. 'third line') then
                call report('hdr.textLine(3) incorrect')
            endif
        endif
!        call report('END: testStdHdr')
        return
        end subroutine testStdHdr
        
        
        subroutine testHdr(hdr)
        type (DRRhdr) hdr, hdr2
!        include 'drrf.h'
!        record /DRRhdr/ hdr, hdr2
        integer MAXROWS, MAXCOLS
        parameter (MAXROWS = 100, MAXCOLS = 50)
        integer rast(MAXCOLS, MAXROWS)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs
        
!        call report('START: testHdr')
        call report('Testing hdr')
        f = DRRopen('drrtesttmp.drr', 'w')
        if (f .eq. 0) call report('testHdr: Unable to open')
        rs = DRRwrHead(f, hdr)
        rs = DRRwrRaster(f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        
        f = DRRopen('drrtesttmp.drr', 'r')
        rs = DRRrdHead(f, hdr2)
        call DRRclose(f)
        
        call compareHdrs(hdr, hdr2)
        
        if (hdr%textLine(1)(:10) .ne. 'first line') then
            call report('hdr.textLine(1) incorrect')
        endif
        if (hdr%textLine(2)(:11) .ne. 'second line') then
            call report('hdr.textLine(2) incorrect')
        endif
        if (hdr%textLine(3)(:10) .ne. 'third line') then
            call report('hdr.textLine(3) incorrect')
        endif
!        call report('END: testHdr')
        return
        end subroutine testHdr
        
        
        subroutine compareHdrs(hdr1, hdr2)
        type (DRRhdr) hdr1, hdr2
!        include 'drrf.h'
!        record /DRRhdr/ hdr1, hdr2
        integer i
        
!        call report('START: compareHdrs')
        if (hdr1%nrows .ne. hdr2%nrows) then
            call report('hdr.nrows mis-match')
        endif
        if (hdr1%ncols .ne. hdr2%ncols) then
            call report('hdr.ncols mis-match')
        endif
        if (hdr1%datatype .ne. hdr2%datatype) then
            call report('hdr.datatype mis-match')
        endif
        if (hdr1%projection .ne. hdr2%projection) then
            call report('hdr.projection mis-match')
        endif
        if (hdr1%bottom .ne. hdr2%bottom) then
            call report('hdr.bottom mis-match')
        endif
        if (hdr1%left .ne. hdr2%left) then
            call report('hdr.left mis-match')
        endif
        if (hdr1%right .ne. hdr2%right) then
            call report('hdr.right mis-match')
        endif
        if (hdr1%top .ne. hdr2%top) then
            call report('hdr.top mis-match')
        endif
        if (hdr1%textLine(1)(:10) .ne. hdr2%textLine(1)(:10)) then
            print*,"hdr1 1st line: ", hdr1%textLine(1)(:10)
            print*,"hdr2 1st line: ", hdr2%textLine(1)(:10)
            call report('hdr.textLine(1) mis-match')
        endif
        if (hdr1%textLine(2)(:11) .ne. hdr2%textLine(2)(:11)) then
            call report('hdr.textLine(2) mis-match')
        endif
        if (hdr1%textLine(3)(:10) .ne. hdr2%textLine(3)(:10)) then
            call report('hdr.textLine(3) mis-match')
        endif
!        call report('END: compareHdrs')
        return
        end subroutine compareHdrs
        
        
        subroutine report(msg)
        character*(*) msg
        write(0, '(a)') msg
        return
        end subroutine report
        
        
        subroutine testInt1(hdr)
        type (DRRhdr) hdr
!        include 'drrf.h'
!        record /DRRhdr/ hdr
        integer MAXROWS, MAXCOLS
        parameter (MAXROWS = 100, MAXCOLS = 50)
        integer*1 rast(MAXCOLS, MAXROWS)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs
        
!        call report('START: testInt1')
        call report('Testing int1')
        hdr%datatype = DRR_UBYTE
        f = DRRopen('drrtesttmp.drr', 'w')
        rs = DRRwrHead(f, hdr)
        rast(14, 27) = 87
        rs = DRRwrRaster(f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        rast(14, 27) = 0
        
        f = DRRopen('drrtesttmp.drr', 'r')
        rs = DRRrdHead(f, hdr)
        rs = DRRrdRaster(1, f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        
        if (rast(14, 27) .ne. 87) then
            call report('testInt1: raster value mis-match')
        endif
!        call report('END: testInt1')
        return
        end subroutine testInt1
        
        
        subroutine testInt2(hdr)
        type (DRRhdr) hdr
!        include 'drrf.h'
!        record /DRRhdr/ hdr
        integer MAXROWS, MAXCOLS
        parameter (MAXROWS = 100, MAXCOLS = 50)
        integer*2 rast(MAXCOLS, MAXROWS)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs
        
!        call report('START: testInt2')
        call report('Testing int2')
        hdr%datatype = DRR_SHORT
        f = DRRopen('drrtesttmp.drr', 'w')
        rs = DRRwrHead(f, hdr)
        rast(14, 27) = 10087
        rs = DRRwrRaster(f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        rast(14, 27) = 0
        
        f = DRRopen('drrtesttmp.drr', 'r')
        rs = DRRrdHead(f, hdr)
        rs = DRRrdRaster(1, f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        
        if (rast(14, 27) .ne. 10087) then
            call report('testInt2: raster value mis-match')
        endif
!        call report('END: testInt2')
        return
        end subroutine testInt2
        
        
        subroutine testInt4(hdr)
        type (DRRhdr) hdr
!        include 'drrf.h'
!        record /DRRhdr/ hdr
        integer MAXROWS, MAXCOLS
        parameter (MAXROWS = 100, MAXCOLS = 50)
        integer rast(MAXCOLS, MAXROWS)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs
        
!        call report('START: testInt4')
        call report('Testing int4')
        hdr%datatype = DRR_LONG
        f = DRRopen('drrtesttmp.drr', 'w')
        rs = DRRwrHead(f, hdr)
        rast(14, 27) = 1000087
        rs = DRRwrRaster(f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        rast(14, 27) = 0
        
        f = DRRopen('drrtesttmp.drr', 'r')
        rs = DRRrdHead(f, hdr)
        rs = DRRrdRaster(1, f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        
        if (rast(14, 27) .ne. 1000087) then
            call report('testInt4: raster value mis-match')
        endif
!        call report('END: testInt4')
        return
        end subroutine testInt4
        
        
        subroutine testReal(hdr)
        type (DRRhdr) hdr
!        include 'drrf.h'
!        record /DRRhdr/ hdr
        integer MAXROWS, MAXCOLS
        parameter (MAXROWS = 100, MAXCOLS = 50)
        real rast(MAXCOLS, MAXROWS)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs
        
!        call report('START: testReal')
        call report('Testing real')
        hdr%datatype = DRR_FLOAT
        f = DRRopen('drrtesttmp.drr', 'w')
        rs = DRRwrHead(f, hdr)
        rast(14, 27) = 487.342
        rs = DRRwrRaster(f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        rast(14, 27) = 0
        
        f = DRRopen('drrtesttmp.drr', 'r')
        rs = DRRrdHead(f, hdr)
        rs = DRRrdRaster(1, f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        
        if (rast(14, 27) .ne. 487.342) then
            call report('testReal: raster value mis-match')
        endif
!        call report('END: testReal')
        return
        end subroutine testReal
        
        
        subroutine testBlock(hdr)
        type (DRRhdr) hdr
!        include 'drrf.h'
!        record /DRRhdr/ hdr
        integer MAXROWS, MAXCOLS, MAXLINESPERTILE
        parameter (MAXROWS = 100, MAXCOLS = 50)
        parameter (MAXLINESPERTILE = 20)
        integer*1 rast(MAXCOLS, MAXROWS)
        integer*1 tile(MAXCOLS, MAXLINESPERTILE)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs, i, j
        
!        call report('START: testBlock')
        call report('Testing block')
        hdr%datatype = DRR_UBYTE
        f = DRRopen('drrtesttmp.drr', 'w')
        rs = DRRwrHead(f, hdr)
        do i=1, MAXCOLS
            do j=1, MAXROWS
                rast(i, j) = 0
            enddo
        enddo
        rast(14, 27) = 87
        rs = DRRwrRaster(f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        
        f = DRRopen('drrtesttmp.drr', 'r')
        rs = DRRrdHead(f, hdr)
        rs = DRRrdBlock(1, 21, 29, 11, 19, f, hdr, tile, MAXCOLS)
        call DRRclose(f)
        
        if (tile(4, 7) .ne. 87) then
            call report('testBlock: raster value mis-match')
        endif
!        call report('END: testBlock')
        return
        end subroutine testBlock
        
        
        subroutine testLine(hdr)
        type (DRRhdr) hdr
!        include 'drrf.h'
!        record /DRRhdr/ hdr
        integer MAXROWS, MAXCOLS, MAXLINESPERTILE
        parameter (MAXROWS = 100, MAXCOLS = 50)
        parameter (MAXLINESPERTILE = 20)
        integer*1 rast(MAXCOLS, MAXROWS)
        integer*1 line(MAXCOLS)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs
        
!        call report('START: testLine')
        call report('Testing line')
        hdr%datatype = DRR_UBYTE
        f = DRRopen('drrtesttmp.drr', 'w')
        rs = DRRwrHead(f, hdr)
        rast(14, 27) = 87
        rs = DRRwrRaster(f, hdr, rast, MAXCOLS)
        call DRRclose(f)
        
        f = DRRopen('drrtesttmp.drr', 'r')
        rs = DRRrdHead(f, hdr)
        rs = DRRrdLine(1, 27, f, hdr, line, MAXCOLS)
        call DRRclose(f)
        
        if (line(14) .ne. 87) then
            call report('testLine: raster value mis-match')
        endif
!        call report('END: testLine')
        return
        end subroutine testLine

        
        
        
        subroutine testNames(hdr)
        type (DRRhdr) hdr
!        include 'drrf.h'
!        record /DRRhdr/ hdr
        integer MAXROWS, MAXCOLS, MAXLINESPERTILE
        parameter (MAXROWS = 100, MAXCOLS = 50)
        parameter (MAXLINESPERTILE = 20)
        integer*1 rast(MAXCOLS, MAXROWS)
!        integer*(C_PTR_SIZE) f
        integer(kind=C_LONG_KIND) f
        integer rs, rasNum
        character*20 name
        
!        call report('START: testNames')
        call report('Testing names')
        hdr%datatype = DRR_UBYTE
        hdr%rastersNamed = 1
        f = DRRopen('drrtesttmp.drr', 'w')
        rs = DRRwrHead(f, hdr)
        rast(14, 27) = 87
        rs = DRRwrNamedRaster(f, hdr, rast, MAXCOLS, 'ras1')
        call DRRclose(f)
        rast(14, 27) = 0
        
        f = DRRopen('drrtesttmp.drr', 'r')
        rs = DRRrdHead(f, hdr)
        rs = DRRrdraster(1, f, hdr, rast, MAXCOLS)
        
        if (rast(14, 27) .ne. 87) then
            call report('testNames: raster value mis-match')
        endif
        
        rasNum = DRRgetRasterNum(f, hdr, 'ras1')
        rs = DRRrdRasterName(rasNum, f, hdr, name)
        if (name(1:4) .ne. 'ras1') then
            call report('testNames: raster name mis-match: '//name)
        endif
        call DRRclose(f)
!        call report('END: testNames')
        return
        end subroutine testNames


        end program testall
