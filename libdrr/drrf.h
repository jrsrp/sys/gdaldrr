C Structure definitions for the header of a DRR file. This is the FORTRAN
C structure definition, and mimics the corresponding C definition. The 
C same structure is used in both languages, without conversion, although
C this relies on the fact that both compilers lay out structures in the same
C way. As it happens they do, so we just use this fact. However, it could be
C a source of portability problems later on.
C
C Neil Flood. 15 Sep 1992.
C
C Modified so that there are only integers (and reals) no integer*2 etc.
C Neil Flood. 19 Jan 1994.
C
        integer MAXIMUM_ALLOWED_RASTERS
        integer NUM_TEXT_LINES, TEXT_LINE_LEN, DATUMNAME_LEN
        parameter (MAXIMUM_ALLOWED_RASTERS = 2000)
        parameter (NUM_TEXT_LINES = 3, TEXT_LINE_LEN = 80)
        parameter (DATUMNAME_LEN = 20)

C       Symbolic names for some of the values in the header
        integer LAT_LONG_PROJ, LCC_PROJ, RAW_IMAGERY_PROJ
        integer DRR_QLD_LL, DRR_QLD_LCC
        integer DRR_UBYTE, DRR_BYTE, DRR_SHORT, DRR_LONG, DRR_FLOAT
        parameter (LAT_LONG_PROJ=0, LCC_PROJ=1)
        parameter (RAW_IMAGERY_PROJ = 2)
        parameter (DRR_QLD_LL=0, DRR_QLD_LCC=1)
        parameter (DRR_UBYTE=0, DRR_BYTE=1, DRR_SHORT=2, DRR_LONG=3)
        parameter (DRR_FLOAT=4)
        
        ! The size of a C long. Must be changed between 32 and 64 bit 
        ! environments. 
        integer C_LONG_SIZE
        parameter (C_LONG_SIZE = 8)
        ! The size of a C pointer. 
        integer C_PTR_SIZE
        parameter (C_PTR_SIZE = 8)


C Cray only.
C       type (DRRhdr) 
        structure /DRRhdr/ 
            integer vmajor
            integer vminor
            integer nrows
            integer ncols
            integer datatype
            integer projection
            real bottom
            real left
            real top
            real right
            integer Nrasters
            integer maxNrasters
            integer linesPerTile
            integer rastersNamed

C Delete all the union stuff for Cray
            union
                map
C These need to be *8 on DEC Alpha, and SGI-64, because they are long's 
C in C (don't know why...). As opposed to other machines where long is *4. 
                    integer*(C_LONG_SIZE) LCCfirstParallel
                    integer*(C_LONG_SIZE) LCCsecondParallel
                    integer*(C_LONG_SIZE) LCCcentralMeridian
                    integer*(C_LONG_SIZE) LCCoriginLatitude
                    integer*(C_LONG_SIZE) LCCfalseEasting
                    integer*(C_LONG_SIZE) LCCfalseNorthing
                    real LCCae
                    real LCCe2
C                   This string length should be DATUMNAME_LEN+1, but
C                   the SGI compiler won't let me.
                    character*21 LCCdatumName
                end map
C               This second map is because FORTRAN won't allow only one map.
C               When we actually get another map part in here, for another
C               projection, this will go.
                map
                    real r
                end map
            end union

C           The following is some padding to make the FORTRAN and C versions 
C           this structure match. The amount of padding required is known
C           simply empirically i.e. this much makes it all line up.
            ! Now compiling on SGI-64, not needed.
            !character padding(3)

C           This string length should be TEXT_LINE_LEN, but
C           the SGI compiler won't let me.
            character*80 textLine(NUM_TEXT_LINES)
            integer imgOffsets(MAXIMUM_ALLOWED_RASTERS)
C Cray only.
C       end type DRRhdr
        end structure


C       Declare the names of the functions, so that when using 
C       no implicit typing we don't get "undeclared function" messages.
        integer*(C_PTR_SIZE) DRRopen
        integer DRRrdHead, DRRrdRaster, DRRwrHead, DRRwrRaster
        integer DRRrdLine, DRRrdBlock, DRRrdRasterName, DRRwrNamedRaster
        integer DRRscoopRaster, DRRunscoopRaster, DRRmodifyHead
        integer DRRgetRasterNum

        external DRRopen
        external DRRclose
        external DRRrdHead, DRRrdRaster, DRRwrHead, DRRwrRaster
        external DRRrdLine, DRRrdBlock, DRRrdRasterName, DRRwrNamedRaster
        external DRRscoopRaster, DRRunscoopRaster, DRRmodifyHead
        external DRRgetRasterNum
