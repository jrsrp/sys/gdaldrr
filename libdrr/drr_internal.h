
#ifndef DRR_INTERNAL_H
#define DRR_INTERNAL_H


#ifdef __cplusplus
extern "C" {
#endif

/* 
  Macros and associated simple function definitions
*/



/* The sizes, in bytes, of the various primitive data types. */
static int drr_datasize[]={1, 1, 2, 4, 4};
/* A macro for accessing the data type sizes. */
#define DRR_DATASIZE(dt) (drr_datasize[(dt)])


/* Little macro to take a (unsigned char *) and convert the two consecutive 
   bytes pointed to from a short (MSB first) to an int in the native format. */
#define XSHORT_TO_INT(p) ((((int)(*(p)))<<8)|((int)*((p)+1)))
/* Similar macro to convert a 4 byte int to a native int */
#define XINT_TO_INT(p) ( (((int)(*(p)))<<24) | (((int)(*((p)+1)))<<16) | \
                        (((int)(*((p)+2)))<<8)  | ((int)(*((p)+3))) )
/* Similar macro for IEEE 32-bit float to native float. */
#if (defined(CRAY_YMP))
/* Assumes the existence of a local variable tmpLong in the routine using
   this macro. */
#define IEEE_FLOAT_TO_FLOAT(d, p)  tmpLong = ((XINT_TO_INT((p)))<<32); \
                                   ieee2cray(&tmpLong, d, 1);
#elif defined(LITTLE_ENDIAN)
#define IEEE_FLOAT_TO_FLOAT(d, p)   *((char *)(d) + 3) = *((char *)p);  \
                                    *((char *)(d) + 2) = *((char *)p+1);\
                                    *((char *)(d) + 1) = *((char *)p+2);\
                                    *((char *)(d))     = *((char *)p+3);
#else
#define IEEE_FLOAT_TO_FLOAT(d, p)  memcpy((d), (p), SIZE_IEEE_FLOAT)
#endif


/* Generic macros to byte-swap 2 & 4 byte words. */
#define BYTE1 0xff000000
#define BYTE2 0x00ff0000
#define BYTE3 0x0000ff00
#define BYTE4 0x000000ff
#define BYTESWAP2(s) ((((short int)(s) & BYTE3) >> 8) | \
                      (((short int)(s) & BYTE4) << 8))
#define BYTESWAP4(s) (((*(int *)(&s) & BYTE1) >> 24) | \
                      ((*(int *)(&s) & BYTE2) >> 8 ) | \
                      ((*(int *)(&s) & BYTE3) << 8 ) | \
                      ((*(int *)(&s) & BYTE4) << 24));


/* Some macros to convert internal values to their external equivalents.
   Take a (unsigned char *p) and a value, and the value is put into the
   place pointed to by p. */
#define INT_TO_XSHORT(p, i) *(p)   = ((unsigned char)((i>>8)&0xff)); \
                            *(p+1) = ((unsigned char)(i&0xff));
#define INT_TO_XINT(p, i)   *(p)   = ((unsigned char)((i>>24)&0xff)); \
                            *(p+1) = ((unsigned char)((i>>16)&0xff)); \
                            *(p+2) = ((unsigned char)((i>>8)&0xff)); \
                            *(p+3) = ((unsigned char)(i&0xff));

#if (defined(CRAY_YMP))
#define FLOAT_TO_IEEE_FLOAT(p, f)  cray2ieee(&tmpLong, &f, 1); \
                                   memcpy(p, &tmpLong, SIZE_IEEE_FLOAT);
#elif (defined(LITTLE_ENDIAN))
#define FLOAT_TO_IEEE_FLOAT(p, f)   *((char *)(p))   = *(((char *)(&(f))) + 3); \
                                    *((char *)(p)+1) = *(((char *)(&(f))) + 2); \
                                    *((char *)(p)+2) = *(((char *)(&(f))) + 1); \
                                    *((char *)(p)+3) = *(((char *)(&(f))));
#else
#define FLOAT_TO_IEEE_FLOAT(p, f)  memcpy(p, &f, SIZE_IEEE_FLOAT);
#endif

/*
  Structure definition for DRRReadXYN pixel <-> tile mapping
*/

typedef struct {
  int numNT;            /* number of N/tileNum combinations */
  int numValues;        /* number of XYN valuies, or max of numXYN*/

  int *ntN;             /* raster numbers, length numNT */
                        /* first raster is 1 */
  int *ntTile;          /* tile numbers, length numNT   */
                        /* first tile is 0. sorry... */
  int *ntNumRowCol;     /* number of rows and cols for each ntN and
                           ntTile, length numNT */

  int **ntRows;         /* rows for this ntT and ntTile, length numNT */
  int **ntCols;         /* cols for this ntT and ntTile, length numNT */
      /* each sub-array is length ntNumRowCol[idxNT]   */
      /* -1 for out-of-range, first pixel is row 0/col 0 */
      
  int **ntXYIdx;        /* index of ntRows/ntCols into original X/Y/N, base 0 */
} NtCombinations;



#ifdef __cplusplus
}
#endif

#endif
