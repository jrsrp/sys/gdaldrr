
Source code for Neil Flood's libdrr.
------------------------------------

This is the code for the libdrr shared library.

The source code for the IRIX(SGI)/CRAY compile was taken from:

/net/jupiter/v0/usr/people/dres/neil/drr

The source for the ALPHA versions was taken from 
cocaine:/usr/people/neilf/slicca/drr.

The source for the PC version (MINGW/CYGWIN) orininally came from the 
same as the ALPHA, but more recently from \\NES12800\raynerd\code\libdrr\

Compilation:
------------

	There is a Makefile for each architecture except CRAY.
	
	Architectures:
	
	Recognized pre-procssor defines are:
	CRAY_YMP, ALPHA, IRIX, MINGW, CYGWIN, SPARC  -> for machine-specific code.
	
	LITTLE_ENDIAN, BIG_ENDIAN  -> for floating-point implementation-specific code. 
	Code should be written for BIG_ENDIAN by default. 
	

IRIX(SGI):

  The n32/o32 should be controlled via the SGI_ABI environment variable, 
  so the Makefile is the same for either.

	> make -f Makefile.IRIX
	
	Makefile.IRIX evan has an install! Thanks Neil...

	> module load [drr|drr32bit|drr64bit]
  > make -f Makefile.IRIX install
  
ALPHA:

	> make -f Makefile.ALPHA libdrr.so

CYGWIN:

	> make -f Makefile.CYGWIN cygdrr.dll

MinGW:

	MinGW is a native non-MS compiler for windows. The easisest way to invoke it
	is on the cygwin command line. Obviously, you need MinGW installed...

	> export PATH=/cygdrive/c/MinGW/bin:${PATH}
	> make -f Makefile.MINGW libdrr.dll

Cleaning up:

	To clean up all .o, .exe, libraries etc for all dists.
	> make -f Makefile.RESTART restart 

 
Testing:
--------

(Note that the Makefile dependencies for these aren't correct - build the 
library explicitly first)

On cygwin, you need to make -f Makefile.CYGWIN <target>.exe - the .exe is required.

These are linked against the library, so make sure that the current
directory is first in the library searchpath. This is automatic under
cygwin(?), but for SGI/APHA you need to do something like:
	
> export LD_LIBRARY_PATH=.:${LD_LIBRARY_PATH}

	
test.exe

	> make -f Makefile.<ARCH> test
	
	
	Test reads a file in and writes it out again:
	
	> ./test in.drr out.drr
  
  There should be no differences in the files
  
  > diff in.drr out.drr
  >
  
drr2ascii.exe

	Use this program to check that the data are meaningfull
	(eg. a max temp raster should have a lot of 80s, which are
	zeros when de-scaled)
	
	Same comments for libraries as above. 
	
	> make -f Makefile.<ARCH> drr2ascii
	> ./drr2ascii in.drr
	80
	80
	.
	.
	.
	
test_drrReadXYN.exe

	Use this to check the optimized pixel extraction routines
	return the correct values.
	You can input coords from stdin, or from a file.
	
	> make -f Makefile.<ARCH> test_drrReadXYN
	> echo 152 -15 | ./test_drrReadXYN 20000015.rai.drr
  hdr read ok
	1 points read
	value valid
	-1, 1
    
test_memory_drrReadXYN.exe

	This is a brute-force memory test - run in a loop, and see if you 
	break something.
	
	> make -f Makefile.<ARCH> test_memory_drrReadXYN
	> ./test_memory_drrReadXYN 20000015.rai.drr 1000 1000
	DRRReadXYN call number:   0   1000 points valid
	DRRReadXYN call number:   1   1000 points valid
	DRRReadXYN call number:   2   1000 points valid
	DRRReadXYN call number:   3   1000 points valid
	DRRReadXYN call number:   4   1000 points valid
	.
	.
	.	
	
