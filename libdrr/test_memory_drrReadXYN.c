/*****************************************

 * test routine

 call as:
 ./test_memory_drrREadXYN filename nPoints nRuns
 
 Extracts nPoints random points from a file
 nRuns times.
 
 max of nPoints is 10000
 
 
*****************************************/
#include <stdio.h>
#include <stdlib.h>
#include "drr.h"

#define MAX_PTS 1000
#define MINMAXRAND(i,j)  (i + (((double)rand())/RAND_MAX)*(j-i))

int main( int argc, char *argv[] )
{
int nPoints,nRuns;
FILE *f;
DRRhdr hdr;

double xpts[MAX_PTS];
double ypts[MAX_PTS];
int count, ptsIdx;
char *values;
int *valid;
int ptsread, zpts = 1;
int numxyz[3];
int nValid = 0;
  
/* check command line */
if( argc != 4 ) {
  fprintf( stderr, "usage: test_memory_drrREadXYN filename nPoints nRuns\n" );
  exit( 1 );  
}


if (sscanf(argv[2],"%d",&nPoints)!=1 || sscanf(argv[3],"%d",&nRuns)!=1) {
  fprintf( stderr, "usage: test_memory_drrREadXYN filename nPoints nRuns\n" );
  exit( 1 );  
}

if (nPoints > MAX_PTS) {
  fprintf( stderr, "maximum allowed nPoints is %d\n", MAX_PTS );
  exit( 1 );  
}


/* open drr file */
f = DRRopen(argv[1], 'r');
DRRrdHead(f, &hdr);


for (count=0; count < nRuns ; count++) {
   
  /* set number of points */
  numxyz[0] = nPoints;
  numxyz[1] = nPoints;
  numxyz[2] = 1;

  for (ptsIdx=0; ptsIdx < nPoints ; ptsIdx++) {
    xpts[ptsIdx] = MINMAXRAND(hdr.left,hdr.right);
    ypts[ptsIdx] = MINMAXRAND(hdr.bottom, hdr.top);
  }

  if( DRRReadXYN(f, &hdr, xpts, ypts, &zpts, numxyz, &values, &ptsread, &valid) != 0 ) {
    exit( 1 );
  }
  
  printf("DRRReadXYN call number: %d   ",count);

  nValid=0;
  for (ptsIdx=0; ptsIdx<nPoints ; ptsIdx++) {
    nValid = nValid + valid[ptsIdx];
  }
  
  printf("%d points valid\n",nValid);
  
  DRRFree(values);
  DRRFree(valid); 

  /*printf("return to continue...\n");
  getchar();*/
}

DRRclose(f);

return 0;

}
