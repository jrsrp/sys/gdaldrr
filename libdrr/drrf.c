/* A set of stub routines to interface bweteen FORTRAN and C for the
   DRR routines. The C routines are found in the file drr.c. These routines
   do no processing, they merely interface between the different 
   data structures.
   Note the naming of the routines here. The names are all in lower case,
   as that is what FORTRAN expects, and they all have (_) appended to their
   name, as that is also expected by FORTRAN. This means that the routines
   can then be called from FORTRAN with what is apparently the same name
   as the corresponding C routine, as well as the same argument list.

   Neil Flood. 14 September 1992.
   
*/
#include <stdio.h>

#define min(a,b) (a>b ? b : a)

#if (defined(CRAY_YMP))
#include <fortran.h>
#include <sys/types.h>
#endif



#include "drr.h"

#if (defined(CRAY_YMP))
#define FN_DRROPEN DRROPEN
#define FN_DRRCLOSE DRRCLOSE
#define FN_DRRRDHEAD DRRRDHEAD
#define FN_DRRRDRASTER DRRRDRASTER
#define FN_DRRSCOOPRASTER DRRSCOOPRASTER
#define FN_DRRWRHEAD DRRWRHEAD
#define FN_DRRMODIFYHEAD DRRMODIFYHEAD
#define FN_DRRWRRASTER DRRWRRASTER
#define FN_DRRUNSCOOPRASTER DRRUNSCOOPRASTER
#define FN_DRRSETDFLTHDR DRRSETDFLTHDR
#define FN_DRRRDLINE DRRRDLINE
#define FN_DRRRDBLOCK DRRRDBLOCK
#define FN_DRRRDRASTERNAME DRRRDRASTERNAME
#define FN_DRRWRNAMEDRASTER DRRWRNAMEDRASTER
#define FN_DRRGETRASTERNUM DRRGETRASTERNUM
#define FN_DRRXY2RC DRRXY2RC
#define FN_DRRRC2XY DRRRC2XY
#define FN_DRRFLOAT2PIX DRRFLOAT2PIX
#define FN_DRRPIX2FLOAT DRRPIX2FLOAT
#else
#define FN_DRROPEN drropen_
#define FN_DRRCLOSE drrclose_
#define FN_DRRRDHEAD drrrdhead_
#define FN_DRRRDRASTER drrrdraster_
#define FN_DRRSCOOPRASTER drrscoopraster_
#define FN_DRRWRHEAD drrwrhead_
#define FN_DRRMODIFYHEAD drrmodifyhead_
#define FN_DRRWRRASTER drrwrraster_
#define FN_DRRUNSCOOPRASTER drrunscoopraster_
#define FN_DRRSETDFLTHDR drrsetdflthdr_
#define FN_DRRRDLINE drrrdline_
#define FN_DRRRDBLOCK drrrdblock_
#define FN_DRRRDRASTERNAME drrrdrastername_
#define FN_DRRWRNAMEDRASTER drrwrnamedraster_
#define FN_DRRGETRASTERNUM drrgetrasternum_
#define FN_DRRXY2RC drrxy2rc_
#define FN_DRRRC2XY drrrc2xy_
#define FN_DRRFLOAT2PIX drrfloat2pix_
#define FN_DRRPIX2FLOAT drrpix2float_
#endif

#if (defined(CRAY_YMP))
FILE *FN_DRROPEN(filename, access)
char filename[];
char *access;
#else
FILE *FN_DRROPEN(filename, access, filenameLen)
char filename[];
int filenameLen;
char *access;
#endif
{
    char *fn;
    FILE *returnVal;
    int i;
#if (defined(CRAY_YMP))
    int filenameLen;

    filenameLen = _fcdlen(filename);
#endif

    fn = (char *)calloc(filenameLen+1, sizeof(char));

    /* Turn the FORTRAN string into a C NULL-terminated string. */
#if (!defined(CRAY_YMP))
/* Fudge the Cray _fcdtocp macro */
#define _fcdtocp(s) (s)
#endif
    strncpy(fn, _fcdtocp(filename), filenameLen);
    i = filenameLen-1;
    while ((fn[i] == ' ') && (i > 0)) i--;
    fn[i+1] = '\0';

    /* Now pass it all on to the C routine. */
    returnVal = DRRopen(fn, *access);

    free(fn);

    return (returnVal);
}


long int FN_DRRRDHEAD(filePtr, hdr)
FILE **filePtr;
DRRhdr *hdr;
{
    long int returnVal;

    returnVal = DRRrdHead((FILE *)(*filePtr), hdr);

    return (returnVal);
}

long int FN_DRRRDRASTER(rasterNum, filePtr, hdr, raster, arrayNcols)
int *rasterNum, *arrayNcols;
FILE **filePtr;
DRRhdr *hdr;
char *raster;
{
    long int returnVal;

    returnVal = DRRrdRaster(*rasterNum, ((FILE *)(*filePtr)), hdr, raster,
	*arrayNcols);

    return (returnVal);
}
long int FN_DRRSCOOPRASTER(rasterNum, filePtr, hdr, raster, nbytes)
int *rasterNum, *nbytes;
FILE **filePtr;
DRRhdr *hdr;
char *raster;
{
    long int returnVal;

    returnVal = DRRscoopRaster(*rasterNum, ((FILE *)(*filePtr)), hdr, raster,
	    nbytes);

    return (returnVal);
}

long int FN_DRRWRHEAD(filePtr, hdr)
FILE **filePtr;
DRRhdr *hdr;
{
    long int returnVal;

    returnVal = DRRwrHead((FILE *)(*filePtr), hdr);

    return (returnVal);
}
long int FN_DRRMODIFYHEAD(filePtr, hdr)
long int *filePtr;
DRRhdr *hdr;
{
    long int returnVal;

    returnVal = DRRmodifyHead((FILE *)(*filePtr), hdr);

    return (returnVal);
}


long int FN_DRRWRRASTER(filePtr, hdr, raster, arrayNcols)
char *raster;
FILE **filePtr;
DRRhdr *hdr;
int *arrayNcols;
{
    long int returnVal;

    returnVal = DRRwrRaster(((FILE *)(*filePtr)), hdr, raster, *arrayNcols);

    return (returnVal);
}

long int FN_DRRUNSCOOPRASTER(filePtr, hdr, raster, nbytes)
char *raster;
FILE **filePtr;
DRRhdr *hdr;
int *nbytes;
{
    long int returnVal;

    returnVal = DRRunScoopRaster(((FILE *)(*filePtr)), hdr, raster, *nbytes);

    return (returnVal);
}

void FN_DRRSETDFLTHDR(hdr, hdrType)
DRRhdr *hdr;
long int *hdrType;
{
    DRRsetDfltHdr(hdr, (int)(*hdrType));
}


void FN_DRRCLOSE(filePtr)
FILE **filePtr;
{
    DRRclose(((FILE *)(*filePtr)));
}
void FN_DRRXY2RC(hdr, x, y, r, c)
DRRhdr *hdr;
float *x, *y;
int *r, *c;
{
    DRRxy2rc(hdr, (double)(*x), (double)(*y), r, c);
}
void FN_DRRRC2XY(hdr, x, y, r, c)
DRRhdr *hdr;
float *x, *y;
int *r, *c;
{
    DRRrc2xy(hdr, x, y, *r, *c);
}
void FN_DRRFLOAT2PIX(raster, hdr, ndx, newVal)
void *raster;
DRRhdr *hdr;
int *ndx;
float *newVal;
{
    DRRfloat2pix(raster, hdr, *ndx, *newVal);
}
float FN_DRRPIX2FLOAT(raster, hdr, ndx)
void *raster;
DRRhdr *hdr;
int *ndx;
{
    float val;

    val = DRRpix2float(raster, hdr, *ndx);

    return (val);
}


int FN_DRRRDLINE(rasterNum, lineNum, filePtr, hdr, line)
int *rasterNum, *lineNum;
FILE **filePtr;
DRRhdr *hdr;
char *line;
{
    long int returnVal;

    returnVal = DRRrdLine(*rasterNum, *lineNum, ((FILE *)(*filePtr)),
	hdr, line);

    return (returnVal);
}


int FN_DRRRDBLOCK(rasterNum, strtLine, endLine, strtCol, endCol, filePtr, hdr, 
	block, arrayNcols)
int *rasterNum, *strtLine, *endLine, *strtCol, *endCol, *arrayNcols;
FILE **filePtr;
DRRhdr *hdr;
char *block;
{
    long int returnVal;

    returnVal = DRRrdBlock(*rasterNum, *strtLine, *endLine, *strtCol, *endCol, 
	((FILE *)(*filePtr)), hdr, block, *arrayNcols);

    return (returnVal);
}


int FN_DRRRDRASTERNAME(rasterNum, filePtr, hdr, name)
int *rasterNum;
FILE **filePtr;
DRRhdr *hdr;
char *name;
{
    long int returnVal;

    returnVal = DRRrdRasterName(*rasterNum, ((FILE *)(*filePtr)),
	hdr, name);

    return (returnVal);
}
#if (defined(CRAY_YMP))
int FN_DRRGETRASTERNUM(filePtr, hdr, name)
long int *filePtr;
DRRhdr *hdr;
char *name;
#else
int FN_DRRGETRASTERNUM(filePtr, hdr, name, nameLen)
long int *filePtr;
DRRhdr *hdr;
char *name;
int nameLen;
#endif
{
    long int returnVal;
    char *localName;
#if (defined(CRAY_YMP))
    int nameLen;

    nameLen = _fcdlen(name);
#endif

    localName = (char *)calloc(nameLen+1, sizeof(char));

    /* Turn the FORTRAN string into a C NULL-terminated string. */
    strncpy(localName, _fcdtocp(name), nameLen);

    returnVal = DRRgetRasterNum(((FILE *)(*filePtr)),
	hdr, localName);

    return (returnVal);
}

#if (defined(CRAY_YMP))
int FN_DRRWRNAMEDRASTER(filePtr, hdr, raster, arrayNcols, name)
char *raster;
FILE **filePtr;
DRRhdr *hdr;
int *arrayNcols;
#else
int FN_DRRWRNAMEDRASTER(filePtr, hdr, raster, arrayNcols, name, nameLen)
char *raster;
FILE **filePtr;
DRRhdr *hdr;
int *arrayNcols;
char *name;
int nameLen;
#endif
{
    long int returnVal;
    char *nameStr;

#if (defined(CRAY_YMP))
    int nameLen;

    nameLen = _fcdlen(name);
#endif

    nameStr = (char *)calloc(RASTERNAME_LEN+1, sizeof(char));

    /* Turn the FORTRAN string into a C NULL-terminated string. */
#if (!defined(CRAY_YMP))
/* Fudge the Cray _fcdtocp macro */
#define _fcdtocp(s) (s)
#endif
    /* Turn the FORTRAN string into a C NULL-terminated string. */
    strncpy(nameStr, name, min(nameLen, RASTERNAME_LEN));
    nameStr[RASTERNAME_LEN+1] = '\0';


    returnVal = DRRwrNamedRaster(((FILE *)(*filePtr)), hdr, raster, *arrayNcols,
	nameStr);

    free(nameStr);

    return (returnVal);
}

