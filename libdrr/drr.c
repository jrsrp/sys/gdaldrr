/* Routines to read and write DRR format raster files. The routines
   provided are

    DRRopen
    DRRrdHead
    DRRrdRaster
    DRRReadXYN
    DRRscoopRaster
    DRRrdLine
    DRRReadTile
    DRRrdBlock
    DRRrdRasterName
    DRRwrHead
    DRRmodifyHead
    DRRwrRaster
    DRRunScoopRaster
    DRRwrNamedRaster
    DRRclose
    DRRsetDfltHead
    DRRxy2rc
    DRRrc2xy
    DRRFree

   The file format was designed by Neil Flood, Tim Danaher and John
   Carter. All the hard work was done by Neil Flood. You can blame
   Dave Rayner for any routine which causes a segfault when returning 0.

   First version - 9 September 1992.

     Moved to QCCA cvs repository January 2005 - see an early version 
     for pre-cvs revision history.

*/



#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include "drr.h"
#include "drr_internal.h"
#include "rle.h"

/**************************************************************
  defines - there are lots more defines in drr_internal.h
*/

#define DRR_VMAJOR 1
#define DRR_VMINOR 0

#define DRR_RECOGNITIONSTR "DRES RASTER V"

#define MAIN_HDR_BLOCK_SIZE 200
#define HDR_BUFF_SIZE (MAIN_HDR_BLOCK_SIZE+NUM_TEXT_LINES*TEXT_LINE_LEN+18)
#define VERNUM_FIELD_WIDTH 5   /* Width (chars) of version number field .*/
#define PROJ_PARAMS_SIZE 60



/***************************************************************
  Internal function declarations
*/

static int checkHdr(DRRhdr *hdr);

static int writeOutHeader(FILE *f,DRRhdr *hdr);

static int drrSetNtCombinations(const double *X, const double *Y, const int *N,
         const int numXYN[], const DRRhdr *hdr, NtCombinations *ntC);

static int drrFreeNtCombinations(NtCombinations *ntC);

static int readTileOffsets(FILE  *f, const DRRhdr *hdr, int4byte **tileOffsets);

static long RasterSize (DRRhdr *hdr);

static long CubeSize   (DRRhdr *hdr);

#if defined(LITTLE_ENDIAN)
void byteSwap(unsigned char *p, int datatype, int n);
#endif

#if (defined(CRAY_YMP))
void ieee2cray(void *ieeeFloat, void *crayFloat, int n);
void cray2ieee(void *ieeeFloat, void *crayFloat, int n);
void convertDRRtoCray(char *tmpRaster, int raster[], int datatype, int numElements);
void convertCrayToDRR(char *tmpRaster, int raster[], int datatype, int numElements);
#endif


/***************************************************************
  External function definitions
*/



FILE *DRRopen(char filename[], char access) {
    char *operation, buff[20];
    FILE *f;
    int error, l;

    error = 0;
    f = NULL;

    switch (access) {
    case 'r':
    case 'a':
        if (access == 'r') {
            f = fopen(filename, "rb");    operation = "reading";
        } else {
            f = fopen(filename, "rb+");   operation = "appending";
        }
        error = (f == NULL);
        if (!error) {
            l = strlen(DRR_RECOGNITIONSTR);
            fread(buff, sizeof(char), l, f);
            error = ferror(f);
        }
        if (!error) {
            error = strncmp(buff, DRR_RECOGNITIONSTR, l) != 0;
            if (error) {
                fprintf(stderr, "File %s is not a DRR file\n", filename);
            }
        }
        break;
    case 'w':
        f = fopen(filename, "wb");   operation = "writing";
        error = (f == NULL);
        break;
    default:
        fprintf(stderr, "Unknown access \"%c\"\n", access);
        break;
    }

    if (error) {
        fprintf(stderr, "Error opening file %s for %s\n", filename, operation);
    }

    if (error && (f != NULL)) fclose(f);
    if (error) f = NULL;

    return (f);
}



int DRRrdHead(FILE *f, DRRhdr *hdr)
{
    char nn[3];
    unsigned char buff[HDR_BUFF_SIZE], *projParams, *tmpBuff;
    int error, i, j;
#if (defined(CRAY_YMP))
    long tmpLong;
#endif

    fseek(f, 0L, SEEK_SET);
    fread(buff, sizeof(char), HDR_BUFF_SIZE, f);
    error = ferror(f);

    if (!error) {
        /* Get out the version number of the format in this file. */
        i = strlen(DRR_RECOGNITIONSTR);
        memcpy(nn, buff+i, 2); nn[2] = '\0';     i += 3;
        sscanf(nn, "%d", &(hdr->vmajor));
        memcpy(nn, buff+i, 2); nn[2] = '\0';     i += 2;
        sscanf(nn, "%d", &(hdr->vminor));

        /* get the rest of the main header block. */
        hdr->nrows = XSHORT_TO_INT(buff+i);  i += SIZE_SHORT;
        hdr->ncols = XSHORT_TO_INT(buff+i);  i += SIZE_SHORT;

        hdr->datatype = (int)(*(buff+i));   i += SIZE_BYTE;

        hdr->projection = (int)(*(buff+i)); i += SIZE_BYTE;
        projParams = buff+i;    i += PROJ_PARAMS_SIZE;


        IEEE_FLOAT_TO_FLOAT(&hdr->bottom, (buff+i));    i += SIZE_IEEE_FLOAT;
        IEEE_FLOAT_TO_FLOAT(&hdr->left  , (buff+i));    i += SIZE_IEEE_FLOAT;
        IEEE_FLOAT_TO_FLOAT(&hdr->top   , (buff+i));    i += SIZE_IEEE_FLOAT;
        IEEE_FLOAT_TO_FLOAT(&hdr->right , (buff+i));    i += SIZE_IEEE_FLOAT;

        hdr->Nrasters = XSHORT_TO_INT(buff+i);       i += SIZE_SHORT;
        hdr->maxNrasters = XSHORT_TO_INT(buff+i);    i += SIZE_SHORT;
        hdr->linesPerTile = (int)(*(buff+i));        i += SIZE_BYTE;
        hdr->rastersNamed = XINT_TO_INT(buff+i);     i += SIZE_INT;

        if (hdr->projection == LCC_PROJ) {
/* This stuff has not yet been converted for the Cray. */
            j = 0;
            hdr->projParam.lcc.firstParallel = *((long *)(projParams+j));
            j += SIZE_LONG;
            hdr->projParam.lcc.secondParallel = *((long *)(projParams+j));
            j += SIZE_LONG;
            hdr->projParam.lcc.centralMeridian = *((long *)(projParams+j));
            j += SIZE_LONG;
            hdr->projParam.lcc.originLatitude = *((long *)(projParams+j));
            j += SIZE_LONG;
            hdr->projParam.lcc.falseEasting = *((long *)(projParams+j));
            j += SIZE_LONG;
            hdr->projParam.lcc.falseNorthing = *((long *)(projParams+j));
            j += SIZE_LONG;
            memcpy(hdr->projParam.lcc.datumName, projParams+j, DATUMNAME_LEN);
        hdr->projParam.lcc.datumName[DATUMNAME_LEN] = '\0';
            j += DATUMNAME_LEN;

        }

        i = MAIN_HDR_BLOCK_SIZE+strlen(DRR_RECOGNITIONSTR)+VERNUM_FIELD_WIDTH;
        for (j=0; j<NUM_TEXT_LINES; j++) {
            memcpy(hdr->textLine[j], buff+i, (TEXT_LINE_LEN-1));
            hdr->textLine[j][TEXT_LINE_LEN-1] = '\0';
            i += TEXT_LINE_LEN;
        }

        tmpBuff = malloc(hdr->maxNrasters*SIZE_LONG);
        fread(tmpBuff, SIZE_LONG, hdr->maxNrasters, f);
        for (j=0; j<hdr->maxNrasters; j++) {
            hdr->imgOffsets[j] = XINT_TO_INT(tmpBuff+j*SIZE_LONG);
        }
        free(tmpBuff);
        error = ferror(f);
    }

    if (!error) {
        error = checkHdr(hdr);
    }

    if (error) {
        fprintf(stderr, "Error reading header\n");
    }

    return (error);
}

int DRRwrHead(FILE *f, DRRhdr *hdr)
{
    int error, i;

    error = checkHdr(hdr);
    if (!error) {
        /* If we are actually writing a new header, then the number of rasters
           must be zero. */
        hdr->Nrasters = 0;
        for (i=0; i<hdr->maxNrasters; i++) hdr->imgOffsets[i] = 0L;

        fseek(f, 0L, SEEK_SET);
        error = writeOutHeader(f, hdr);
    }

    return (error);
}

int DRRmodifyHead(FILE *f,DRRhdr *hdr)
{
    int error;

    error = checkHdr(hdr);
    if (!error) {
        fseek(f, 0L, SEEK_SET);
        error = writeOutHeader(f, hdr);
    }

    return (error);
}


/* Read a raster from an open DRR file. The raster number is the index,
   starting at 1, of the raster, withing the file. i.e. the first raster
   in the file is raster number 1.
   The raster is read in to the memory area pointed to by "raster".
   The variable arrayNcols is use to correctly index the buffer as a
   two-dimensional array, allowing FORTRAN to pass down the declared size
   of the array (the first dimension) and receive the raster back in a
   sensible way.
*/
int DRRrdRaster(int rasterNum, FILE *f, DRRhdr *hdr, char *raster, int arrayNcols)
{
    long rasSize, seekto;
    struct stat fstatbuf;
    char *rasBuff, *crPtr, *rPtr;
    int error, rInc, elementSize, compressedSize, r, imghdrSize;
#if (defined(CRAY_YMP))
    char *tmpRaster;
#endif

    rasBuff = NULL;

    error = checkHdr(hdr);

    /* Check that the desired raster actually exists in the file. */
    if (!error) error = (rasterNum > hdr->Nrasters);
    if (!error) error = (rasterNum < 1);

    /* Check that the array appears to have enough room. */
    if (!error) error = (arrayNcols < hdr->ncols);

    if (!error) {
        /* First find the size of the raster, on disk. This is the difference
           between the offsets of this raster and the next one, or the end of
           the file if this is the last raster. */
        if (rasterNum < hdr->Nrasters) {
            rasSize = hdr->imgOffsets[rasterNum] - hdr->imgOffsets[rasterNum-1];
        } else {
            fstat(fileno(f), &fstatbuf);
            rasSize = fstatbuf.st_size - hdr->imgOffsets[rasterNum-1];
        }
        imghdrSize = (((hdr->nrows-1)/hdr->linesPerTile) + 1)*SIZE_LONG;
        rasSize = rasSize - imghdrSize;

        /* Now malloc enough memory to hold the whole compressed raster. */
        rasBuff = (char *)malloc(rasSize);
        error = (rasBuff == NULL);
    }

    /* Seek to the start of the raster, skipping the tile offsets, as we don't
       need them when we read the whole raster. */
    if (!error) {
        seekto = hdr->imgOffsets[rasterNum-1];
        seekto = seekto + imghdrSize;
        fseek(f, seekto, SEEK_SET);
        error = ferror(f);
    }

    /* Read in the whole compressed raster into a single block of memory. */
    if (!error) {
        fread(rasBuff, sizeof(char), rasSize, f);
        error = ferror(f);
    }

    if (!error) {
        /* Now uncompress the raster, storing it in the buffer 'raster'. */
        crPtr = rasBuff;
        elementSize = DRR_DATASIZE(hdr->datatype);
#if (defined(CRAY_YMP))
        tmpRaster = malloc(hdr->ncols*hdr->nrows*elementSize);
        rPtr = tmpRaster;
#else
        rPtr = raster;
#endif
        rInc = arrayNcols*elementSize;
        for (r=0; r<hdr->nrows; r++) {
            drrUncompress(rPtr, hdr->ncols, crPtr, elementSize, &compressedSize);
            crPtr += compressedSize;
            rPtr += rInc;
        }

#if (defined(CRAY_YMP))
        convertDRRtoCray(tmpRaster, (void *)raster, hdr->datatype,
            (hdr->nrows*hdr->ncols));
        free(tmpRaster);
#elif (defined(LITTLE_ENDIAN))
        /* need arrayNcols in case we get a Fortran array larger than the actual raster. */
        /* this should probably be done line by line with correct skips, as in drrRdBlock.
         * But this works.  */
        byteSwap((void *)raster, hdr->datatype, (hdr->nrows*arrayNcols));
#endif
    }

    if (rasBuff != NULL) free(rasBuff);

    return  (error);
}


/* Routine which reads in the compressed raster, without uncompressing it.
   This is intended for use in the drrappend program, along
   with DRRunScoopRaster.
   If the rasters are named, this also scoops the name up with it.
*/
int DRRscoopRaster(int rasterNum, FILE *f, DRRhdr *hdr, char *raster, int *nbytes)
{
    long rasSize, seekto;
    struct stat fstatbuf;
    int error;
    /* this doesnt appear to be used int imghdrSize; */

    error = checkHdr(hdr);

    /* Check that the desired raster actually exists in the file. */
    if (!error) error = (rasterNum > hdr->Nrasters);
    if (!error) error = (rasterNum < 1);

    if (!error) {
        /* First find the size of the raster, on disk. This is the difference
           between the offsets of this raster and the next one, or the end of
           the file if this is the last raster. */
        if (rasterNum < hdr->Nrasters) {
            rasSize = hdr->imgOffsets[rasterNum] - hdr->imgOffsets[rasterNum-1];
        } else {
            fstat(fileno(f), &fstatbuf);
            rasSize = fstatbuf.st_size - hdr->imgOffsets[rasterNum-1];
        }

        /* This line doesnt appear to be used, so I commented it
        imghdrSize = (((hdr->nrows-1)/hdr->linesPerTile) + 1)*SIZE_LONG;
        */

        if (hdr->rastersNamed) rasSize = rasSize + RASTERNAME_LEN;
        *nbytes = rasSize;
    }

    /* Seek to the start of the raster, skipping the tile offsets, as we don't
       need them when we read the whole raster. */
    if (!error) {
        seekto = hdr->imgOffsets[rasterNum-1];
        if (hdr->rastersNamed) seekto = seekto - RASTERNAME_LEN;
        fseek(f, seekto, SEEK_SET);
        error = ferror(f);
    }

    /* Read in the whole compressed raster into a single block of memory. */
    if (!error) {
        fread(raster, sizeof(char), rasSize, f);
        error = ferror(f);
    }


    return  (error);
}

/* This dumps out a "scooped" raster, as a byte stream, and updates the
   header to reflect the extra raster. It is essentially the same
   as DRRwrRaster, but the compress part is left out, as this is assumed
   to have already happened.
   This routine is intended only for use in conjunction with DRRscoopRaster,
   in the drrappend program.
*/
int DRRunScoopRaster(FILE *f, DRRhdr *hdr, char *raster, int nbytes)
{
    int error;
    long imgOffset;
    struct stat fstatbuf;

    /* Check that we can add another raster to this file, without re-writing. */
    error = (hdr->Nrasters >= hdr->maxNrasters);

    if (!error) {
        /* Find the offset of the end of the file. */
        fstat(fileno(f), &fstatbuf);
        imgOffset = fstatbuf.st_size;
    }

    if (!error) {
        /* Write out the raster, complete with tile offsets. Note that
           the variable 'tileOffset' currently holds the total size of the
           raster, including the little raster header block. */
        fseek(f, imgOffset, SEEK_SET);
        fwrite(raster, sizeof(char), nbytes, f);
        error = ferror(f);
        fflush(f);
    }

    if (!error) {
        /* Now update the header to reflect the new raster. */
        hdr->Nrasters++;
        if (hdr->rastersNamed) {
            hdr->imgOffsets[hdr->Nrasters-1] = imgOffset + RASTERNAME_LEN;
        } else {
            hdr->imgOffsets[hdr->Nrasters-1] = imgOffset;
        }

        fseek(f, 0L, SEEK_SET);
        error = writeOutHeader(f, hdr);
    }

    return (error);
}

int DRRwrRaster(FILE *f, DRRhdr *hdr, char *raster, int arrayNcols)
{
    char *rasBuff, *p, *ras, *oPtr;
    int elementSize, rInc, r, error, compressedSize;
    long buffSize, tileOffset, imgOffset;
    struct stat fstatbuf;
#if (defined(CRAY_YMP))
    char *tmpRaster;
#endif

    rasBuff = NULL;

    /* Check that we can add another raster to this file, without re-writing. */
    error = (hdr->Nrasters >= hdr->maxNrasters);

    /* Check that the array appears to have enough room. */
    if (!error) error = (arrayNcols < hdr->ncols);

    if (!error) {
        /* Find the offset of the end of the file. */
        fstat(fileno(f), &fstatbuf);
        imgOffset = fstatbuf.st_size;

        elementSize = DRR_DATASIZE(hdr->datatype);

        /* malloc enough space to hold the entire raster in uncompressed form,
           complete with flags for each row. This is the worst case scenario. */
        buffSize = hdr->nrows*(hdr->ncols*elementSize + 1);
        /* Allow room for the tile offsets. */
        buffSize += (((hdr->nrows-1)/hdr->linesPerTile) + 1)*SIZE_LONG;
        rasBuff = (char *)malloc(buffSize);
        error = (rasBuff == NULL);
    }

    if (!error) {

#if (defined(CRAY_YMP))
        tmpRaster = malloc(hdr->ncols*hdr->nrows*elementSize);
        convertCrayToDRR(tmpRaster, (void *)raster, hdr->datatype,
            (hdr->nrows*hdr->ncols));
        ras = tmpRaster;
#elif defined(LITTLE_ENDIAN)
        /* need arrayNcols in case we get a Fortran array larger than the actual raster. */
        byteSwap((unsigned char *)raster, hdr->datatype, (hdr->nrows*arrayNcols));
        ras = raster;
#else
        ras = raster;
#endif

        /* Loop over each row, compressing and storing in rasBuff. */
        oPtr = rasBuff;
        tileOffset = (((hdr->nrows-1)/hdr->linesPerTile) + 1)*SIZE_LONG;
        p = rasBuff + tileOffset;
        rInc = arrayNcols*elementSize;
        for (r=0; r<hdr->nrows; r++) {
            drrCompress(ras, hdr->ncols, p, elementSize, &compressedSize);
            if ((r % hdr->linesPerTile) == 0) {
                /* Store a tile offset. */
                INT_TO_XINT(oPtr, tileOffset);
                oPtr += SIZE_LONG;
            }
            tileOffset += compressedSize;
            p += compressedSize;
            ras += rInc;
        }

        /* Write out the raster, complete with tile offsets. Note that
           the variable 'tileOffset' currently holds the total size of the
           raster, including the little raster header block. */
        fseek(f, imgOffset, SEEK_SET);
        fwrite(rasBuff, sizeof(char), tileOffset, f);
        error = ferror(f);
        fflush(f);

#if (defined(CRAY_YMP))
        free(tmpRaster);
#endif
    }

    if (!error) {
        /* Now update the header to reflect the new raster. */
        hdr->Nrasters++;
        hdr->imgOffsets[hdr->Nrasters-1] = imgOffset;

        fseek(f, 0L, SEEK_SET);
        error = writeOutHeader(f, hdr);
    }

    if (rasBuff != NULL) free(rasBuff);

    return (error);
}



void DRRsetDfltHdr(DRRhdr *hdr, int hdrType)
{
    hdr->vmajor = DRR_VMAJOR;
    hdr->vminor = DRR_VMINOR;
    hdr->maxNrasters = 500;
    hdr->Nrasters = 0;
    hdr->linesPerTile = 20;
    memset(hdr->imgOffsets, 0, MAXIMUM_ALLOWED_RASTERS*SIZE_INT);
    memset(hdr->textLine, 0, NUM_TEXT_LINES*TEXT_LINE_LEN);

    switch (hdrType) {
    case DRR_QLD_LL:
        hdr->nrows = 373;
        hdr->ncols = 313;
        hdr->projection = LAT_LONG_PROJ;
        hdr->bottom = -29.15;
        hdr->left = 138.0;
        hdr->top = -10.55;
        hdr->right = 153.6;
        break;
    case DRR_QLD_LCC:
        hdr->projParam.lcc.ae = 6378160.0;
        hdr->projParam.lcc.e2 = 0.6694542E-2;
        strcpy(hdr->projParam.lcc.datumName, "AGD84");
        /* Standard parallels are -14 deg and -26 deg, in seconds. */
        hdr->projParam.lcc.firstParallel = -14*3600;
        hdr->projParam.lcc.secondParallel = -26*3600;
        hdr->projParam.lcc.originLatitude =
            hdr->projParam.lcc.secondParallel;
        /* Central meridian is 146 deg, in seconds. */
        hdr->projParam.lcc.centralMeridian = 146*3600;
        hdr->projParam.lcc.falseEasting = 0;
        hdr->projParam.lcc.falseNorthing = 0;
        break;
    case DRR_AUS_LL:
        hdr->nrows = 681;
        hdr->ncols = 841;
        hdr->projection = LAT_LONG_PROJ;
        hdr->bottom = -44.0;
        hdr->left = 112.0;
        hdr->top = -10.0;
        hdr->right = 154.0;
        break;
    default: break;
    }
}


/* The main purpose of the routine is to ensure that a FORTRAN program is
   able to correctly close a file opened by C. */
void DRRclose(FILE *f)
{
    if (f != NULL) {
        fclose(f);
    }
}



/* This routine reads a single line from a single raster of a DRR file.
   As with DRRrdRaster, the raster number starts at 1. The line number
   also starts at 1.
*/
int DRRrdLine(int rasterNum, int lineNum, FILE *f, DRRhdr *hdr, char *line)
{
    long int imgOffset, tileOffset;
    long *tileOffsetBuff;
    char *tileBuff, *p;
    int numTiles, tileNum, tileSize, elementSize, firstLine, i, error;
    unsigned char *tmp_tileOffsetBuff;
    int compressedSize;
    struct stat fstatbuf;
    unsigned char *tmpLine;

    tileOffsetBuff = NULL;
    tileBuff = NULL;

    error = (lineNum > hdr->nrows);
    if (!error) error = (rasterNum > hdr->Nrasters);
    if (!error) error = (rasterNum < 1);
    if (!error) error = (lineNum < 1);


    if (!error) {
        numTiles = ((hdr->nrows-1)/hdr->linesPerTile) + 1;

        tileOffsetBuff = (long *)malloc(numTiles*sizeof(long));
        tmp_tileOffsetBuff = (unsigned char *)malloc(numTiles*SIZE_LONG);

        /* First read the image header, containing the tile offsets. */
        imgOffset = hdr->imgOffsets[rasterNum-1];
        fseek(f, imgOffset, SEEK_SET);
        fread(tmp_tileOffsetBuff, SIZE_LONG, numTiles, f);

        for (i=0; i<numTiles; i++) {
            tileOffsetBuff[i] = XINT_TO_INT(tmp_tileOffsetBuff+i*SIZE_LONG);
        }
        free(tmp_tileOffsetBuff);


        /* Figure out which tile contains the line in question, and what offset
           in the file it starts at.
           Note that tileNum runs from 0 .. (numTiles-1).*/
        tileNum = (lineNum-1)/hdr->linesPerTile;
        /* The tile offsets are relative to the start of the image, make it
           relative to the start of the file. */
        tileOffset = tileOffsetBuff[tileNum] + imgOffset;

        /* Calculate the size of the tile, so that we know how much to
           read in. The size is the difference between the offset of this
           one and the offset of the next one, or end-of-file. */
        if (tileNum < (numTiles-1)) {
            tileSize = tileOffsetBuff[tileNum+1] + imgOffset - tileOffset;
        } else {
            if (rasterNum < hdr->Nrasters) {
                tileSize = hdr->imgOffsets[rasterNum] - tileOffset;
            } else {
                fstat(fileno(f), &fstatbuf);
                tileSize = fstatbuf.st_size - tileOffset;
            }
        }

        /* Malloc enough space for the tile. */
        tileBuff = (char *)malloc(tileSize);

        /* Now read in the required tile. */
        fseek(f, tileOffset, SEEK_SET);
        fread(tileBuff, sizeof(char), tileSize, f);

        /* Now uncompress the tile, to get at the desired line. Note that we
           must uncompress the earlier lines in the tile, but these are stored
           in the same buffer, line, and then overwritten. The desired line
           is the last one uncompressed, and this is left in the "line" buffer
           when the loop terminates. */
        firstLine = tileNum*hdr->linesPerTile;
        elementSize = DRR_DATASIZE(hdr->datatype);
        p = tileBuff;

#if (defined(CRAY_YMP))
        tmpLine = (unsigned char *)malloc(hdr->ncols*elementSize);
#else
        tmpLine = (unsigned char *)line;
#endif

        for (i=firstLine; i<lineNum; i++) {
            drrUncompress((char *)tmpLine, hdr->ncols, p, elementSize, &compressedSize);
            p += compressedSize;
        }

#if (defined(CRAY_YMP))
        convertDRRtoCray(tmpLine, (void *)line, hdr->datatype, hdr->ncols);
        free(tmpLine);
#elif defined(LITTLE_ENDIAN)
        /* This does not need the fortran array fix applied elsewhere, as it works on a single line. */
        byteSwap((unsigned char *)line, hdr->datatype, hdr->ncols);
#endif
    }

    if (tileBuff != NULL) free(tileBuff);
    if (tileOffsetBuff != NULL) free(tileOffsetBuff);

    return (error);
}



int DRRReadXYN(FILE  *filePtr, const DRRhdr *hdr, const double *X, const double *Y, const int *N,
         const int numXYN[], char **xyValues, int *numValues, int **valuesValid) {
  /*
     Read a list of X/Y/N (long/lat/Raster_Number) from a file.

     Inputs:

     FILE  *filePtr          - open drr file descriptor. Doesnt matter where it is.

     cont DRRhdr *hdr        - drr hdr, already filled in.

     const  double *X        - coordinates to read, in projection units
     const  double *Y          (ie lat, long, raster number)
     const  int    *N          Any array may be length 1, in which case
                               it is treated as the same for all coords.

     const  int numXYN[]     - number of values in X, Y, N arrays.

     Outputs:

     char   **xyValues       - values for drr for pixels specified by X,Y,N.

                               for drrs with datatypes > 1 byte, data can be read as
                               consecutive bytes.
                               ie. it should have the correct byte order for
                               your system!

                               memory for xyValues is assigned internally.
                               please free it with DRRFree when you are done!

      int *numValues         - The number of xyValues, or max of numXYN.
                               Not very useful...

      int  **valuesValid    -  valuesValid is 1 for valid X/Y/N combinations,
                               0 for out-of-range combinations.

                               Memory for valuesVaid is allocated internally.
                               please free it with DRRFree when you are done!


      Return value is non-zero on failure.

*/

/*
2003-09-10 DPR
2004-04-28 DPR change all sub-routines to pass &hdr. General tidyup.
2004-07-19 DPR return valuesValid.
*/

  NtCombinations ntC;  /* N/Tile to X/Y mappings, so we dont have to read the same
                           tile twice */


  int idxNT;            /* loop variables */
  int *thisRows;
  int *thisCols;
  int *thisXYIdx;
  int  idxNtNumRowCol,bidx;

  int  r,c,n,idx,tileNum;        /* tmp row/col/N/index/tileNum */

  char *tileData;                /* native format array of data for a tile
                                    ie consecutive chars for shorts,longs, etc. */

  int  elementSize;              /* number of bytes for each data value */
  char *xyValues_internal;       /* the  data for each X/Y, in native format */
  int  *valuesValid_internal;   /* 1 for in-range data */

  /* get the N/Tile -> X/Y mappings */
  if (drrSetNtCombinations(X, Y, N, numXYN, hdr, &ntC) != 0) {
    return(1);
  }

  /* allocate memory for return data */

  elementSize = (int) DRR_DATASIZE(hdr->datatype);
  xyValues_internal=(char *)malloc(ntC.numValues * elementSize);
  valuesValid_internal=(int *)malloc(ntC.numValues * sizeof(int));


  /* load each N/tile we need */
  for (idxNT=0 ; idxNT < ntC.numNT ; idxNT++) {
    n=ntC.ntN[idxNT];          /* raster number we need */
    tileNum=ntC.ntTile[idxNT]; /* tile number we need */

    thisRows  = ntC.ntRows[idxNT];  /* rows for this n and tileNum */
    thisCols  = ntC.ntCols[idxNT];  /* cols for this n and tileNum */
    thisXYIdx = ntC.ntXYIdx[idxNT]; /* indices of original X/Y/N, base 0,
                                       for thisRows and thisCols*/


    if ((tileNum <= -1) || (n > hdr->Nrasters) || (n <= 0)) {
      /*
        X/Y/N combination out-of-range. set as invalid
      */
      for (idxNtNumRowCol = 0 ; idxNtNumRowCol < ntC.ntNumRowCol[idxNT] ; idxNtNumRowCol++) {
         idx = thisXYIdx[idxNtNumRowCol];
         valuesValid_internal[idx]=0;
      }
    } else {
      if (DRRReadTile(filePtr, hdr, tileNum, n, &tileData) !=0) {
        return(1);
      }

      /* select the values for each row/col for this tile, and assign to xyValues */
      for (idxNtNumRowCol = 0 ; idxNtNumRowCol < ntC.ntNumRowCol[idxNT] ; idxNtNumRowCol++) {
        /* row in the tile: */
        r   = thisRows[idxNtNumRowCol] % hdr->linesPerTile;
        /* col in the tile: */
        c   = thisCols[idxNtNumRowCol];
        /* index into xyValues */
        idx = thisXYIdx[idxNtNumRowCol];

        /* allocate one byte at a time */
        for (bidx=0 ; bidx < elementSize ; bidx++) {
          *(xyValues_internal + bidx + idx*elementSize) =
              *(tileData + bidx + (r*hdr->ncols +c) *elementSize);
        }

        /* and set this as a good value */
        valuesValid_internal[idx]=1;
      }

      DRRFree(tileData);
    }
  }

  *numValues=ntC.numValues;

  *xyValues=xyValues_internal; /* save the addresses of the allocated memory */
  *valuesValid=valuesValid_internal;

  drrFreeNtCombinations(&ntC);

  return(0);

}


int DRRReadTile(FILE  *f, const DRRhdr *hdr, int tileNum, int rasterNum, char **tileData) {
/*
     Read a tile from a drr file.

     Inputs:

     FILE  *f          - open drr file descriptor. Doesnt matter where it is.

     const DRRhdr *hdr - drr hdr, already filled in.

     int tileNum       - the number of the tile to read.
                         Note that tileNum runs from 0 .. (numTiles-1).

     int rasterNum     - the raster number (plane) to get the tile from.
                         Note that the first raster is 1, not 0.

     Outputs:

     char **tileData   - the values for the tile.
                         first row first, then second row, etc...

                         for tiles with data types > 1 byte, data can be read as
                         consecutive bytes ie. it should have the correct byte order for
                         your system!

                         memory allocated internally -
                         Please free with DRRFree when you are done!!

     Return value is non-zero on failure.

*/

/*
2003-09-09 DPR Adapted from DRRrdLine in drr.c
2003-12-04 DPR bugfix bounds checking on tileNum
2004-04-28 DPR change all sub-routines to pass &hdr. General tidyup.
*/

    long int  imgOffset, tileOffset;
    int4byte      *tileOffsetBuff;
    char      *tileBuff, *p;
    int       numTiles,  tileSize, elementSize, firstLine, lastLine, numLines, i;
    int       compressedSize;

    char      *thisTileData; /* internal tile data array */

    int       tidx;  /* loop variable */

    struct stat fstatbuf;


    tileOffsetBuff = NULL;
    tileBuff = NULL;


    numTiles = ((hdr->nrows-1)/hdr->linesPerTile) + 1;

    if (rasterNum < 1) {
      fprintf(stderr,"ERROR: rasterNumber < 1\n");
      return(1);
    } else if (tileNum < 0) {
      fprintf(stderr,"ERROR: row/col out-of range\n");
      return(1);
    } else if (tileNum >= numTiles) {
      fprintf(stderr,"ERROR: requested tileNum not available\n");
      return(1);
    } else if (rasterNum > hdr->Nrasters) {
      fprintf(stderr,"ERROR: requested rasterNum not available\n");
      return(1);
    }


    /* First get the tile offsets.  */

    /* where are the tile offsets for this raster? */
    imgOffset = hdr->imgOffsets[rasterNum-1];

    /* move to start of the tile offsets */
    fseek(f, imgOffset, SEEK_SET);

    /* read them in */
    if (readTileOffsets(f, hdr, &tileOffsetBuff) != 0) {
      return(1);
    }

    /* The tile offsets are relative to the start of the image,
       Get the tile offset for this tile
       relative to the start of the file. */
    tileOffset = tileOffsetBuff[tileNum] + imgOffset;


    /* Calculate the size of the tile, so that we know how much to
       read in. The size is the difference between the offset of this
       one and the offset of the next one, or end-of-file. */
    if (tileNum < (numTiles-1)) {
      tileSize = tileOffsetBuff[tileNum+1] + imgOffset - tileOffset;
    } else {
      if (rasterNum < hdr->Nrasters) {
        tileSize = hdr->imgOffsets[rasterNum] - tileOffset;
      } else {
        fstat(fileno(f), &fstatbuf);
        tileSize = fstatbuf.st_size - tileOffset;
      }
    }

    DRRFree(tileOffsetBuff);

    /* Malloc enough space for the tile. */
    tileBuff = (char *)malloc(tileSize);
    if (tileBuff == NULL) {
      fprintf(stderr,"ERROR: unable to allocate memory\n");
      return(1);
    }

    /* Now read in the required tile. */
    fseek(f, tileOffset, SEEK_SET);
    fread(tileBuff, sizeof(char), tileSize, f);

    /* allocate space for uncompressed tile */
    firstLine = tileNum*hdr->linesPerTile;

    lastLine = (tileNum+1)*hdr->linesPerTile -1;
    if (lastLine >= hdr->nrows) {
      lastLine = hdr->nrows -1;
    }

    numLines = (lastLine - firstLine + 1);

    elementSize = (int) DRR_DATASIZE(hdr->datatype);

    thisTileData = (char *)malloc(numLines * hdr->ncols * elementSize);


    /* Now uncompress the tile. */

    p = tileBuff;

    tidx=0;
    for (i=firstLine; i<=lastLine; i++) {

      /* read the data into thisTileData, get the compressedSize */
      drrUncompress(thisTileData + (tidx *hdr->ncols * elementSize), hdr->ncols, p, elementSize, &compressedSize);

      /* increment pointer by compressedSize */
      p += compressedSize;

      /* increment row counter */
      tidx++;
    }


    /* byteSwap on PC endian */
#if defined(LITTLE_ENDIAN)
    /* F90 array fix applied elsewhere is not needed here, as byteswap is not 
     * working here on memory used as a fortran array, but rather on locally malloc'd memory
     * thisTileData, whose starting address is written into tileData for the calling routine to access.
     * In fact, this whole routine should probably not be called from fortran. There is no wrapper call
     * in drrf.c.
     */
    byteSwap((unsigned char *)thisTileData, hdr->datatype,  numLines * hdr->ncols);
#endif

    free(tileBuff);

    *tileData = thisTileData;  /* save the address of the allocated memory */

    return (0);
}


/* Routine to read a block of pixels from a DRR file, by reading only those
   tiles which are needed.
   Reads in the required tiles, and uncompresses them into an array. The
   desired block is then copied out of this array. This double copy is
   necessary because the format dictates that we must read in and uncompress
   a whole tile at a time. Thus we need the extra copy operation because we
   cannot uncompress directly into the memory we are trying to fill. It could
   probably be done in a more memory efficient way than reading the whole lot
   into one large array, by doing the double copy on each line as it comes
   in, but that is not a serious problem for us, as we are always (hopefully)
   working in physical memory, not virtual.
   As with the other DRRrd routines, the raster number and row and column
   numbers start from 1.
*/
int DRRrdBlock(int rasterNum, int strtLine, int endLine, int strtCol,
    int endCol, FILE *f, DRRhdr *hdr, char *block, int arrayNcols)
{
    int strtTile, endTile, error, numTiles, tile, tileSize, i, destLineSize;
    int elementSize, compressedSize, lineSize, fullLineSize, l, firstLine;
    long imgOffset, *tileOffsetBuff, arraySize, tileOffset;
    unsigned char *tmp_tileOffsetBuff;
    char *array, *line, *p, *b, *tileBuff;
    struct stat fstatbuf;

    array = NULL;
    tileOffsetBuff = NULL;

    /* Do all the necessary range checking. */
    error = (rasterNum < 1);
    if (!error) error = (rasterNum > hdr->Nrasters);
    if (!error) error = (strtLine > endLine);
    if (!error) error = (strtLine < 1);
    if (!error) error = (endLine > hdr->nrows);
    if (!error) error = (strtCol > endCol);
    if (!error) error = (strtCol < 1);
    if (!error) error = (endCol > hdr->ncols);

    if (!error) {
        /* If the block is really the whole raster then just read it using the
           standard rdRaster routine, as this is more efficient. Only if it
           really is a sub-block should we actually use this rdBlock approach.
        */
        if ((strtLine == 1) && (endLine == hdr->nrows) && (strtCol == 1) &&
            (endCol == hdr->ncols)) {
            error = DRRrdRaster(rasterNum, f, hdr, block, arrayNcols);
        } else {
            /* Find the appropriate tiles. Note that the tile numbers used here
               run from 0..(numTiles-1). */
            strtTile = (strtLine-1)/hdr->linesPerTile;
            endTile = (endLine-1)/hdr->linesPerTile;


            numTiles = ((hdr->nrows-1)/hdr->linesPerTile) + 1;

            tileOffsetBuff = (long *)malloc(numTiles*sizeof(long));
            tmp_tileOffsetBuff = (unsigned char *)malloc(numTiles*SIZE_LONG);

            /* First read the image header, containing the tile offsets. */
            imgOffset = hdr->imgOffsets[rasterNum-1];
            fseek(f, imgOffset, SEEK_SET);
            fread(tmp_tileOffsetBuff, SIZE_LONG, numTiles, f);

            for (i=0; i<numTiles; i++) {
                tileOffsetBuff[i] = XINT_TO_INT(tmp_tileOffsetBuff+i*SIZE_LONG);
            }
            free(tmp_tileOffsetBuff);


            /* Allocate space for the piece of the raster containing all
               the whole tiles we need. */
            arraySize = (endTile-strtTile+1)*hdr->linesPerTile*
                        hdr->ncols*DRR_DATASIZE(hdr->datatype);
            array = (char *) malloc(arraySize);

            /* The tile offsets are relative to the start of the
               image, make it relative to the start of the file. */
            tileOffset = tileOffsetBuff[strtTile] + imgOffset;

            line = array;

            for (tile=strtTile; tile<=endTile; tile++) {
                /* Calculate the size of the tile, so that we know how much to
                   read in. The size is the difference between the offset of
                   this one and the offset of the next one, or end-of-file. */
                if (tile < (numTiles-1)) {
                    tileSize = tileOffsetBuff[tile+1] + imgOffset - tileOffset;
                } else {
                    if (rasterNum < hdr->Nrasters) {
                        tileSize = hdr->imgOffsets[rasterNum] - tileOffset;
                    } else {
                        fstat(fileno(f), &fstatbuf);
                        tileSize = fstatbuf.st_size - tileOffset;
                    }
                }

                /* Malloc enough space for the tile. */
                tileBuff = (char *)malloc(tileSize);

                /* Now read in the required tile. */
                fseek(f, tileOffset, SEEK_SET);
                fread(tileBuff, sizeof(char), tileSize, f);

                /* Now uncompress the whole tile. */
                elementSize = DRR_DATASIZE(hdr->datatype);
                p = tileBuff;
                for (i=0; ((i<hdr->linesPerTile)&&(p<(tileBuff+tileSize))); i++) {
                    drrUncompress(line, hdr->ncols, p, elementSize, &compressedSize);
                    p += compressedSize;
                    line += hdr->ncols*elementSize;
                }

                if (tileBuff != NULL) free(tileBuff);


                tileOffset += tileSize;
            }

            /* Now copy from array to block. */

            /* Calculate the line numbers (starting at zero) to use as indexes
               in the array. */
            firstLine = strtLine - (strtTile*hdr->linesPerTile) - 1;
            p = array + firstLine*hdr->ncols*elementSize +
                (strtCol-1)*elementSize;

            b = block;
            lineSize = (endCol-strtCol+1)*elementSize;
            fullLineSize = hdr->ncols*elementSize;
#if (defined(CRAY_YMP))
            /* Everything is the same size on a Cray. */
            destLineSize = arrayNcols*sizeof(long);
#else
            destLineSize = arrayNcols*elementSize;
#endif
            for (l=(strtLine-1); l<endLine; l++) {
#if (defined(CRAY_YMP))
                convertDRRtoCray(p, (void *)b, hdr->datatype, (endCol-strtCol+1));
#elif (defined(LITTLE_ENDIAN))
                /* This works for fortran arrays I think, because of the value of destLineSize. */
                byteSwap((unsigned char *)p, hdr->datatype, (endCol-strtCol+1));
                memcpy(b, p, lineSize);
#else
                memcpy(b, p, lineSize);
#endif
                p += fullLineSize;
                b += destLineSize;
            }



            if (tileOffsetBuff != NULL) free(tileOffsetBuff);
            if (array != NULL) free(array);
        }
    }

    return (error);
}



int DRRrdRasterName(int rasterNum, FILE *f, DRRhdr *hdr, char name[])
{
    long int seekto;
    int error;

    error = 0;

    /* Check that the desired raster actually exists in the file. */
    if (!error) error = (rasterNum > hdr->Nrasters);
    if (!error) error = (rasterNum < 1);


    /* Check that the raster header says we have raster names. */
    if (!error) error = !(hdr->rastersNamed);

    if (!error) {
        /* Calculate the offset of the raster name. */
        seekto = hdr->imgOffsets[rasterNum-1] - RASTERNAME_LEN;

        fseek(f, seekto, SEEK_SET);
        error = ferror(f);
    }

    if (!error) {
        fread(name, 1, RASTERNAME_LEN, f);
        error = ferror(f);
    }

    return (error);
}

/* Trim trailing blanks. */
void trim(char str[])
{
    int i;
    i = strlen(str)-1;
    while ((i>0) && (str[i] == ' ')) {
        str[i] = '\0';
        i--;
    }
    if (str[0] == ' ') str[0] = '\0';
}

int DRRgetRasterNum(FILE *f, DRRhdr *hdr, char name[])
{
    int i, found, error, rs;
    char rastName[RASTERNAME_LEN+1];

    if (!hdr->rastersNamed) return(0);

    rastName[RASTERNAME_LEN] = '\0';

    found = 0;
    error = 0;
    i = 1;
    while ((!found) && (!error) && (i <= hdr->Nrasters)) {
        rs = DRRrdRasterName(i, f, hdr, rastName);
        error = (rs != 0);
        trim(rastName);
        found = (strcmp(rastName, name) == 0);

        if (!(found || error)) i++;
    }

    if (found) {
        return (i);
    } else {
        return (0);
    }
}



/* This routine writes a named raster layer. The writes the
   raster in exactly the same way as an unnamed one, but puts in
   a character string of <RASTERNAME_LEN> bytes before the raster itself.
   Note that the imgOffset is still calculated by the wrRaster routine,
   after the name has been written, so that it points to the byte AFTER
   the name. This means that rdRaster can be used without modification
   on files with named rasters.
*/
int DRRwrNamedRaster(FILE *f,DRRhdr *hdr,char *raster,int arrayNcols,char name[])
{
    int error, i;
    char tmp[RASTERNAME_LEN+1];

    if (hdr->rastersNamed) {
        for (i=0; i<RASTERNAME_LEN+1; i++) tmp[i] = '\0';
        strncpy(tmp, name, RASTERNAME_LEN);
        /* Go to the end of the file, and write the name. */
        fseek(f, (0L), SEEK_END);
        fwrite(tmp, 1, RASTERNAME_LEN, f);
        fflush(f);
    }

    error = DRRwrRaster(f, hdr, raster, arrayNcols);

    return (error);
}

/* #define NINT(r) ((int)((r)+0.5))  This does not work properly if r is negative!  Learn a better method, you! */

void DRRxy2rc(DRRhdr *hdr, double x, double y, int *r, int *c)
{
    float xres, yres;

    xres = (hdr->right - hdr->left)/(hdr->ncols - 1);
    yres = (hdr->top - hdr->bottom)/(hdr->nrows - 1);
    if ((y >= hdr->bottom-yres/2) && (y <= hdr->top+yres/2) &&
        (x >= hdr->left-xres/2) && (x <= hdr->right+xres/2)) {
        *r = ceilf( ( hdr -> top - (float) y ) / yres + 0.5 );
        *c = ceilf( ( (float) x - hdr->left) / xres + 0.5 ); 
    } else {
        *r = 0;
        *c = 0;
    }

}
void DRRrc2xy(DRRhdr *hdr, float *x, float *y, int r, int c)
{
    float xres, yres;

    if ( r>=1 && r<=hdr->nrows && c>=1 && c<=hdr->ncols ) {
        xres = (hdr->right - hdr->left) / (hdr->ncols - 1);
        yres = (hdr->top - hdr->bottom) / (hdr->nrows - 1);
        *x = hdr->left + (c-1) * xres;
        *y = hdr->top - (r-1) * yres;
    } else {
        *x = 1<<30;  
        *y = 1<<30;  /*  to signify errors. */
    }
}



void DRRFree(void *ptr) {
  /*
     Free memory allocated by mallocs in the drr library.

     unix/cygwin, routines dont care about this, but
     windows/mingw does.

     In windows/mingw, the free must be called in the dll
     which called malloc

     Inputs:

     void *ptr    - memory which needs to be freed.


  */

  free(ptr);
}


/***************************************************************
  Internal function definitions
*/


static int writeOutHeader(FILE *f, DRRhdr *hdr)
{
    int error, i;
    unsigned char buff[HDR_BUFF_SIZE+MAXIMUM_ALLOWED_RASTERS*4], *p, *projParams, *pp;
    char versionStr[VERNUM_FIELD_WIDTH+1];

#if (defined(CRAY_YMP))
    long tmpLong;
#endif

    /* Clear the buffer. */
    memset(buff, 0, HDR_BUFF_SIZE+MAXIMUM_ALLOWED_RASTERS*4);

    /* Human-readable string to say it is a drr file */
    p = buff;
    memcpy(p, DRR_RECOGNITIONSTR, strlen(DRR_RECOGNITIONSTR));
    p += strlen(DRR_RECOGNITIONSTR);

    /* First fill in the current version number... */
    hdr->vmajor = DRR_VMAJOR;
    hdr->vminor = DRR_VMINOR;

    /* ... then put it in a string... */
    sprintf(versionStr,"%02d.%02d", hdr->vmajor, hdr->vminor);

    /* ... then put it in the buff. We all love C. */
    for (i=0; i < strlen(versionStr); i++) {
       *p = (unsigned char)versionStr[i];
       p++;
        }

    INT_TO_XSHORT(p, hdr->nrows);            p += SIZE_SHORT;
    INT_TO_XSHORT(p, hdr->ncols);            p += SIZE_SHORT;

    *p = (unsigned char)hdr->datatype;      p+= SIZE_BYTE;
    *p = (unsigned char)hdr->projection;    p+= SIZE_BYTE;

    projParams = p;
    p += PROJ_PARAMS_SIZE;

    FLOAT_TO_IEEE_FLOAT(p, hdr->bottom);    p += SIZE_IEEE_FLOAT;
    FLOAT_TO_IEEE_FLOAT(p, hdr->left);      p += SIZE_IEEE_FLOAT;
    FLOAT_TO_IEEE_FLOAT(p, hdr->top);       p += SIZE_IEEE_FLOAT;
    FLOAT_TO_IEEE_FLOAT(p, hdr->right);     p += SIZE_IEEE_FLOAT;

    INT_TO_XSHORT(p, hdr->Nrasters);                 p += SIZE_SHORT;
    INT_TO_XSHORT(p, hdr->maxNrasters);              p += SIZE_SHORT;
    *p = (unsigned char)hdr->linesPerTile;           p += SIZE_BYTE;
    INT_TO_XINT(p, hdr->rastersNamed);               p += SIZE_INT;

    if (hdr->projection == LCC_PROJ) {
/* This stuff has not yet been converted for the Cray. */
        pp = projParams;
        memcpy(pp, &hdr->projParam.lcc.firstParallel, sizeof(long));
        pp += sizeof(long);
        memcpy(pp, &hdr->projParam.lcc.secondParallel, sizeof(long));
        pp += sizeof(long);
        memcpy(pp, &hdr->projParam.lcc.centralMeridian, sizeof(long));
        pp += sizeof(long);
        memcpy(pp, &hdr->projParam.lcc.originLatitude, sizeof(long));
        pp += sizeof(long);
        memcpy(pp, &hdr->projParam.lcc.falseEasting, sizeof(long));
        pp += sizeof(long);
        memcpy(pp, &hdr->projParam.lcc.falseNorthing, sizeof(long));
        pp += sizeof(long);
        memcpy(p, hdr->projParam.lcc.datumName, DATUMNAME_LEN);
        pp += DATUMNAME_LEN;

    }

    p = buff + MAIN_HDR_BLOCK_SIZE + strlen(DRR_RECOGNITIONSTR) +
        VERNUM_FIELD_WIDTH;
    for (i=0; i<NUM_TEXT_LINES; i++) {
        memcpy(p, hdr->textLine[i], TEXT_LINE_LEN);
        p += TEXT_LINE_LEN;
    }

    for (i=0; i<hdr->maxNrasters; i++) {
        INT_TO_XINT(p+i*SIZE_LONG, hdr->imgOffsets[i]);
    }

    fseek(f, 0L, SEEK_SET);
    fwrite(buff, sizeof(char), HDR_BUFF_SIZE+hdr->maxNrasters*SIZE_LONG, f);
    error = ferror(f);
    fflush(f);

    return (error);
}

static int checkHdr(DRRhdr *hdr)
{
    int error, i;

    error = (hdr->nrows <= 0);
    if (!error) {
        error = (hdr->ncols <= 0);
    }
    if (!error) {
        error = ((hdr->datatype < 0) || (hdr->datatype > 4));
    }
    if (!error) {
        error = ((hdr->projection < 0) || (hdr->projection > 2));
    }
    if (!error) {
        if (hdr->projection != RAW_IMAGERY_PROJ) {
            error = (hdr->bottom >= hdr->top);
        }
    }
    if (!error) {
        error = (hdr->left >= hdr->right);
    }
    if (!error) {
        error = (hdr->Nrasters > hdr->maxNrasters);
    }

    for (i=0; ((i<hdr->Nrasters) && (!error)); i++) {
        error = (hdr->imgOffsets[i] <= 0L);

    }

    return (error);
}


static int drrSetNtCombinations(const double *X, const double *Y,
  const int *N, const int numXYN[], const DRRhdr *hdr,
  NtCombinations *ntC) {
/*
     Determine N/Tile to X/Y mappings. Basically, we only want to load
     a given N/Tile once, so we need to work out which N/Tile combinations
     are needed, and which X/Y/N are on each N/Tile.

     const  double *X        - coordinates to read, in projection units
     const  double *Y          (ie lat, long, raster number)
     const  int    *N          Any array may be length 1, in which case
                               it is treated as the same for all coords.

     const  int numXYN[]     - number of values in X, Y, N arrays.

     const DRRhdr *hdr       - drr hdr specifying coordinate system.

     NtCombinations *ntC     - N/Tile to X/Y mappings.

                               if X or Y are outsize the limits, ntC.ntTile=-1

                               Memory for ntC is allocated internally;
                               call drrFreeNtCombinations when you are done.

     Return value is non-zero on failure.

*/

/*
2003-09-09 DPR
2004-04-28 DPR change all sub-routines to pass &hdr. General tidyup.
*/

/* Because I copied this, I assign output varaibles to NtCombinations at the end */

  int   numValues; /* length *values, or max of numXYN*/
  int   idxXYN;    /* counter */
  int   idxValues; /* counter */

  int *rows, *cols;     /* row/col for X,Y, padded to length numValues  */
  int *inN;             /* copy fo N, padded to length numValues     */
  int *inTileNum;       /* tile numbers, length numValues */

  double  dblx, dbly; /* tmp vars */

  int     r,c,n,tileNum;        /* tmp row/col/N/tileNum */


  /*
     variables for finding the N/tile combinations and associated row/col
  */

  int numNT;            /* number of N/tileNum combinations */

  int *ntN;             /* raster numbers, length numNT */
  int *ntTile;          /* tile numbers, length numNT   */
  int *ntNumRowCol;     /* number of rows and cols for each ntN and
         ntTile, length numNT */

  int **ntRows;         /* rows for this ntT and ntTile, length numNT */
  int **ntCols;         /* cols for this ntT and ntTile, length numNT */
      /* each sub-array is length ntNumRowCol[idxNT]   */

  int **ntXYIdx;        /* index of ntRows/ntCols into original X/Y/N */

  int idxNT;            /* loop variables */
  int *thisRows;        /* loop variables */
  int *thisCols;
  int *thisXYIdx;



  /*
     work out rows/cols/N
     have to expand if any are length 1
  */

  /* find the max numXYN */
  numValues=0;
  for (idxXYN = 0 ; idxXYN < 3 ; idxXYN++) {
    if (numXYN[idxXYN] > numValues) {
      numValues = numXYN[idxXYN];
    }
  }

  /* check we have something */
  if (numValues <= 0) {
    fprintf(stderr,"WARNING: numXYN were all zero or negative in DRRReadXYN\n");
    return(2);
  }

  /*
     assign memory for rows, cols, inN
  */

  /* rows */
  if ((rows=(int *) malloc(numValues * sizeof(double))) == NULL) {
    fprintf(stderr,"%s\n","ERROR: unable to allocate memory for rows");
    return(1);
  }

  /* cols */
  if ((cols=(int *) malloc(numValues * sizeof(double))) == NULL) {
    fprintf(stderr,"%s\n","ERROR: unable to allocate memory for cols");
    free(rows);
    return(1);
  }

  /* inN */
  if ((inN=(int *) malloc(numValues * sizeof(int))) == NULL) {
    fprintf(stderr,"%s\n","ERROR: unable to allocate memory for inN");
    free(rows);
    free(cols);
    return(1);
  }

  /* tileNumber */
  if ((inTileNum=(int *) malloc(numValues * sizeof(int))) == NULL) {
    fprintf(stderr,"%s\n","ERROR: unable to allocate memory for tileNum");
    free(rows);
    free(cols);
    free(inN);
    return(1);
  }

  /*
     calculate the row/col/inN indices foreach X,Y
  */

  for (idxValues=0 ; idxValues < numValues ; idxValues++) {

    /*
       Get the x,y for this idxValues

       If there are less than numValues, use
       the first one for every idxValues
    */

    if (numXYN[0] < numValues) {
      dblx=X[0];
    } else {
      dblx=X[idxValues];
    }

    if (numXYN[1] < numValues) {
      dbly=Y[0];
    } else {
      dbly=Y[idxValues];
    }

    if (numXYN[2] < numValues) {
      n=N[0];
    } else {
      n=N[idxValues];
    }

    /* Explicitly cast away the const-ness of the DRRhdr pointer */
    DRRxy2rc((DRRhdr *)hdr, dblx, dbly, &r, &c);

    /* r and c are 0 on out-of-range */
    /* we transform to C base-0, so now they are -1 for out-of-range */
    r--;
    c--;

    /*  find the required tile number   */
    if ((r == -1) || (c == -1)) {
      tileNum = -1;
    } else {
      tileNum = (r)/hdr->linesPerTile;
    }



    /* assign to memory */

    rows[idxValues]=r;
    cols[idxValues]=c;
    inN[idxValues]=n;
    inTileNum[idxValues]=tileNum;

    /* printf("%d %d %d %d\n",*(rows+idxValues),*(cols+idxValues),*(inN+idxValues),*(inTileNum+idxValues)); */

  }

  /*
     foreach N/tile required find the corresponding
     row/col indices
  */


  numNT=0;
  for (idxValues=0 ; idxValues < numValues ; idxValues++) {
    r=rows[idxValues];
    c=cols[idxValues];
    n=inN[idxValues];
    tileNum=inTileNum[idxValues];

    if (numNT == 0) {
      /* allocate new memory */
      ntN         = (int *) malloc(sizeof(int));
      ntTile      = (int *) malloc(sizeof(int));
      ntNumRowCol = (int *) malloc(sizeof(int));

      thisRows    = (int *) malloc(sizeof(int));
      thisCols    = (int *) malloc(sizeof(int));
      thisXYIdx   = (int *) malloc(sizeof(int));

      ntRows   = (int **) malloc(sizeof(int*));
      ntCols   = (int **) malloc(sizeof(int*));
      ntXYIdx  = (int **) malloc(sizeof(int*));

      /* assign actual data */
      *ntN=n;
      *ntTile=tileNum;
      *ntNumRowCol=1;

      *thisRows=r;
      *thisCols=c;
      *thisXYIdx=idxValues;

      *ntRows=thisRows;
      *ntCols=thisCols;
      *ntXYIdx=thisXYIdx;

      numNT++;

    } else {
      for (idxNT=0 ; idxNT < numNT ; idxNT++) {

        if ((*(ntN+idxNT)==n) && (*(ntTile+idxNT)==tileNum)) {
          /* we already know that we have to load this tile/N  */
          /* so just add the r anc c */

          /* increase memory */
          thisRows=ntRows[idxNT];
          thisRows=realloc(thisRows,(ntNumRowCol[idxNT]+1) *  sizeof(int));
          thisCols=ntCols[idxNT];
          thisCols=realloc(thisCols, (ntNumRowCol[idxNT]+1)*  sizeof(int));
          thisXYIdx=ntXYIdx[idxNT];
          thisXYIdx=realloc(thisXYIdx, (ntNumRowCol[idxNT]+1)*  sizeof(int));

          /* no guarantee that realloc left array in the same place */
          ntRows[idxNT] = thisRows;
          ntCols[idxNT] = thisCols;
          ntXYIdx[idxNT] = thisXYIdx;

          /* assign data */
          thisRows[ntNumRowCol[idxNT]] = r;
          thisCols[ntNumRowCol[idxNT]] = c;
          thisXYIdx[ntNumRowCol[idxNT]] = idxValues;

          /* increment ntNumRowCol */
          ntNumRowCol[idxNT] = ntNumRowCol[idxNT] + 1;


          break;
        }
      }

      if (idxNT==numNT) {
        /* we didn't find this tile/N */

        /* allocate new memory */
        ntN         = realloc(ntN, (numNT+1) * sizeof(int));
        ntTile      = realloc(ntTile, (numNT+1) * sizeof(int));
        ntNumRowCol = realloc(ntNumRowCol, (numNT+1) * sizeof(int));

        thisRows    = (int *) malloc(sizeof(int));
        thisCols    = (int *) malloc(sizeof(int));
        thisXYIdx   = (int *) malloc(sizeof(int));

        ntRows      = realloc(ntRows, (numNT+1) * sizeof(int*));
        ntCols      = realloc(ntCols, (numNT+1) * sizeof(int*));
        ntXYIdx     = realloc(ntXYIdx, (numNT+1) * sizeof(int*));


        /* assign actual data */
        ntN[numNT]=n;
        ntTile[numNT]=tileNum;
        ntNumRowCol[numNT]=1;

        ntRows[numNT]=thisRows;
        ntCols[numNT]=thisCols;
        ntXYIdx[numNT]=thisXYIdx;


        *thisRows=r;
        *thisCols=c;
        *thisXYIdx=idxValues;

        numNT++;

      }
    }
  }

  /*
    Assign all return variables to ntC
  */


  ntC->numNT       = numNT;
  ntC->numValues   = numValues;
  ntC->ntN     = ntN;
  ntC->ntTile  = ntTile;
  ntC->ntNumRowCol = ntNumRowCol;
  ntC->ntRows  = ntRows;
  ntC->ntCols  = ntCols;
  ntC->ntXYIdx = ntXYIdx;

  if (rows != NULL) free(rows);
  if (cols != NULL) free(cols);
  if (inN != NULL) free(inN);
  if (inTileNum != NULL) free(inTileNum);

  return(0);
}

static int drrFreeNtCombinations(NtCombinations *ntC) {
/*
  Free memory allocated ny drrSetNtCombinations.

  NtCombinations *ntC     - N/Tile to X/Y mappings. Input/output.
*/

  int ntidx;

  for (ntidx=0 ; ntidx < ntC->numNT ; ntidx++) {
      if (ntC->ntRows[ntidx] !=NULL) free(ntC->ntRows[ntidx]);
      if (ntC->ntCols[ntidx] !=NULL) free(ntC->ntCols[ntidx]);
      if (ntC->ntXYIdx[ntidx] !=NULL) free(ntC->ntXYIdx[ntidx]);
  }

  if (ntC->ntN !=NULL) free(ntC->ntN);
  if (ntC->ntTile !=NULL) free(ntC->ntTile);
  if (ntC->ntNumRowCol !=NULL) free(ntC->ntNumRowCol);

  return(0);
}


static int readTileOffsets(FILE  *f, const DRRhdr *hdr, int4byte **tileOffsets) {
/*

     Read the byte offsets of the tiles from the from a drr file.

     Inputs:

     FILE  *f           - open drr file descriptor. This should be pointing
                          to the start of the raster for which you want the
                          tile offsets.

                          On return, it will be set to the start of the raster data.

     const DRRhdr *hdr  - drr hdr, must be already filled in.

     Outputs:

     int4byte **tileOffsets - return data. The file offsets, to the start of each
                          tile, relative to the start of the raster header.

                          There are
                             numTiles = ((hdr->nrows-1)/hdr->linesPerTile) + 1;
                          values.

                          Memory for tileOffsets is allocated internallly -
                          Please free with DRRFree when you are done!!

     Return value is non-zero on failure.

*/

    /*
      first, we need to read in the offsets as 4 chars
    */

    int4byte  *int_tileOffsetBuff;
    int  numTiles;
    int i;
    size_t    nRead;  /* fread return type */
    int tmpInt;

    numTiles = ((hdr->nrows-1)/hdr->linesPerTile) + 1;

    /* allocate some space to read the data into */
    int_tileOffsetBuff = (int4byte *)malloc(numTiles*SIZE_LONG);

    /* read in the offsets as 4 char chunks */
    nRead = fread(int_tileOffsetBuff, SIZE_LONG, numTiles, f);

    /* check read status */
    if (nRead==0) {
      fprintf(stderr,"ERROR: unable to read tile offsets");
      return(1);
    }

    /* make sure byte-order is correct for this machine */
    for (i=0; i<numTiles; i++) {
      tmpInt = XINT_TO_INT((unsigned char *)(int_tileOffsetBuff +i));
      int_tileOffsetBuff[i] = (int4byte)tmpInt;
    }


    /* set return data */
    *tileOffsets = int_tileOffsetBuff;

    /* we are done */
    return(0);
}

static long RasterSize(DRRhdr *hdr) {
  /*
     Size of a single plane of the raster cube, as determiend
     by hdr.

     Inputs:

     DRRhdr *hdr  -  header with input data

     Returns number of bytes in a raster plane.

  */

  return(hdr->nrows * hdr->ncols * DRR_DATASIZE(hdr->datatype));
}


static long CubeSize(DRRhdr *hdr) {
  /*
     Size of the entire 3D raster cube, as determined by hdr

     Size of a single plane of the raster cube, as determiend
     by hdr.

     Inputs:

     DRRhdr *hdr  -  header with input data

     Returns number of bytes in a raster cube.
  */

  return(RasterSize(hdr) * hdr->Nrasters);
}


#if defined(LITTLE_ENDIAN)
/* This is not fortran array aware.  It works if the fortran array is the right size, or 1d. */
void byteSwap(unsigned char *p, int datatype, int n)
{
    int i;
    short int *sip;
    int *ip;

    switch (datatype) {
        case DRR_UBYTE:
        case DRR_BYTE:
            break;
        case DRR_SHORT:
            sip = (short int *)p;
            for (i=0; i<n; i++) {
            sip[i] = BYTESWAP2(sip[i]);
            }
            break;
        case DRR_LONG:
        case DRR_FLOAT:
            ip = (int *)p;
            for (i=0; i<n; i++) {
            ip[i] = BYTESWAP4(ip[i]);
            }
            break;
    }
}
#endif



#if (defined(CRAY_YMP))

fortran int IEG2CRAY(int *, int *, void *, int *, void *, int *);
fortran int CRAY2IEG(int *, int *, void *, int *, void *, int *);

void ieee2cray(void *ieeeFloat, void *crayFloat, int n)
{
    int type, bitoff, stride, ier;

    type = 2;
    bitoff = 0;
    stride = 1;

    ier = IEG2CRAY(&type, &n, ieeeFloat, &bitoff, crayFloat, &stride);
}


void cray2ieee(void *ieeeFloat, void *crayFloat, int n)
{
    int type, bitoff, stride, ier;

    type = 2;
    bitoff = 0;
    stride = 1;

    ier = CRAY2IEG(&type, &n, ieeeFloat, &bitoff, crayFloat, &stride);
}


void convertDRRtoCray(char *tmpRaster, int raster[], int datatype, int numElements)
{
    int i;
    unsigned char *p;

    switch (datatype) {
        case DRR_UBYTE:
        case DRR_BYTE:
            for (i=0; i<numElements; i++) {
                raster[i] = tmpRaster[i];
            }
            break;
        case DRR_SHORT:
            for (i=0; i<numElements; i++) {
                raster[i] = XSHORT_TO_INT(tmpRaster+2*i);
            }
            break;
        case DRR_LONG:
            for (i=0; i<numElements; i++) {
                raster[i] = XINT_TO_INT(tmpRaster+4*i);
            }
            break;
        case DRR_FLOAT:
            p = malloc(4*numElements);
            memcpy(p, tmpRaster, 4*numElements);
            ieee2cray(tmpRaster, raster, numElements);
            free(p);
            break;
    }
}

void convertCrayToDRR(char *tmpRaster, int raster[], int datatype, int numElements)
{
    int i;

    switch (datatype) {
        case DRR_UBYTE:
        case DRR_BYTE:
            for (i=0; i<numElements; i++) {
                tmpRaster[i] = raster[i];
            }
            break;
        case DRR_SHORT:
            for (i=0; i<numElements; i++) {
                INT_TO_XSHORT(tmpRaster+2*i, raster[i]);
            }
            break;
        case DRR_LONG:
            for (i=0; i<numElements; i++) {
                INT_TO_XINT(tmpRaster+2*i, raster[i]);
            }
            break;
        case DRR_FLOAT:
            cray2ieee(tmpRaster, raster, numElements);
            break;
    }
}

#endif
