# Makefile for DRR library. This Makefile is as required by the Remote Sensing Centre,
# who maintain their own build of it, built from the QCCCE-maintained source code. 
# The decision to do this was taken by Neil Flood, but since he was the person who 
# originally wrote DRR in the first place, he really has to shoulder a lot more 
# blame than one might think. 
# Neil Flood. 

.SUFFIXES:	.f90 .c .o .exe

#################################

MACH_TYPE=-DLINUX
SYS5INCLUDE=
BYTE_ORDER_DEF=-DLITTLE_ENDIAN

DEFS = $(MACH_TYPE) $(SYS5INCLUDE) $(BYTE_ORDER_DEF)

FLAGS=-g -O2 -fPIC
F90FLAGS=-g -O2 -fPIC

LD_FLAGS=-L./

INCS=-I.

# dont include libdrr.so here, as this is used to compile it!
LIBES=-lc

CC=gcc
F90C=gfortran

CFLAGS=$(FLAGS) $(DEFS) $(INCS)

#
# basic compile commands
#

%.o : %.c 
	${CC} $(CFLAGS) -c -o $@ $<

%: %.o 
	$(CC) $(CFLAGS) -o $*.exe $*.o  $(LD_FLAGS) $(LIBES) libdrr.so -lm

.f90.o:
	$(F90C) $(F90FLAGS) -c $<


#
# list of dependencies
#

default:	all

all:	libdrr.so drrf90.o
 
# .o

drr.o:  drr.c drr.h
 
rle.o:  rle.c rle.h
 
drrconv.o: drrconv.c drr.h
 
drrf.o: drrf.c drrf.h

# .so

DRROBJS=drr.o rle.o drrf.o drrconv.o

libdrr.so: $(DRROBJS) drr.c rle.c drrf.c drrconv.c  
	$(CC) $(CFLAGS) $(LD_FLAGS) -shared -o libdrr.so $(DRROBJS)
   
# .exe 	

test:	test.o libdrr.so

drr2ascii: drr2ascii.o libdrr.so 

test_readXYN: test_readXYN.o libdrr.so 

test_memory_drrReadXYN: test_memory_drrReadXYN.o libdrr.so 


install:	all
	if [ ! -e $(DRR_LIB_PATH) ]; then mkdir -p $(DRR_LIB_PATH); fi
	cp libdrr.so $(DRR_LIB_PATH)
	if [ ! -e $(DRR_INCLUDE_PATH) ]; then mkdir -p $(DRR_INCLUDE_PATH); fi
	cp drr.h drrf.h $(DRR_INCLUDE_PATH)
	if [ ! -e $(DRR_F90MOD_PATH) ]; then mkdir -p $(DRR_F90MOD_PATH); fi
	cp drr.mod $(DRR_F90MOD_PATH)

clean:
	rm -f *.o *.a *.so *.mod



