#include <stdio.h>

#include "drr.h"

float DRRpix2float(void *raster, DRRhdr *hdr, int pix)
{
    float val;

    switch (hdr->datatype) {
        case DRR_FLOAT:
            val = ((float *)raster)[pix];
            break;
        case DRR_LONG:
            val = (float)(((int *)raster)[pix]);
            break;
        case DRR_SHORT:
            val = (float)(((short int *)raster)[pix]);
            break;
        case DRR_BYTE:
            val = (float)(((signed char *)raster)[pix]);
        case DRR_UBYTE:
            val = (float)(((unsigned char *)raster)[pix]);
            break;
    }

    return (val);
}

void DRRfloat2pix(void *raster, DRRhdr *hdr, int pix, float val)
{
    switch (hdr->datatype) {
        case DRR_FLOAT:
            ((float *)raster)[pix] = val;
            break;
        case DRR_LONG:
            ((int *)raster)[pix] = (int)val;
            break;
        case DRR_SHORT:
            ((short int *)raster)[pix] = (short int)val;
            break;
        case DRR_BYTE:
            ((signed char *)raster)[pix] = (signed char)val;
        case DRR_UBYTE:
            ((unsigned char *)raster)[pix] = (unsigned char)val;
            break;
    }
}
