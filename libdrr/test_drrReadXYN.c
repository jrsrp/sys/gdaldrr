/*****************************************

 * test routine

 call as:

 cat | ./test_drrREadXYN filename.drr

  or

 ./test_drrREadXYN filename.drr xypoints

 stdin is a list of:

 xpt1 ypt1
 xpt2 ypt2
 .
 .
 .


*****************************************/
#include <stdio.h>
#include "drr.h"

#define MAX_PTS 100000

int main( int argc, char *argv[] )
{
double xpts[MAX_PTS];
double ypts[MAX_PTS];
int count;
char *values;
int *valid;
int ptsread, N = 1;
int numxyn[3];

FILE *f, *pointsf;
DRRhdr hdr;

char *ras;
unsigned char *uRas;
short int *sRas;
int *iRas;
float *fRas;

int nret;


  /* check command line */
  if ((argc != 2) && (argc != 3)) {
    fprintf( stderr, "usage:\n"
                     "xpt ypt | test_drrREadXYN drrfilename\n"
                     "test_drrREadXYN drrfilename xypointsfilename\n" );
    exit( 1 );
  }

  /* read in the points */
  count = 0;

  if (argc == 2) {
    pointsf = stdin;
  } else {
    pointsf = fopen(argv[2],"rt");
    if (NULL == pointsf) {
        fprintf( stderr, "Can not open file %s\n", argv[2] );
        exit( 1 );
    }
  }

  while( ! feof( pointsf ) )   {
    if( count >= MAX_PTS )    {
      fprintf( stderr, "Too many points!\n" );
      exit( 1 );
    }
    nret = fscanf( pointsf, "%lf %lf\n", &xpts[count], &ypts[count] );
    if (nret != 2) {
      break;
    }
    count++;
  }

  if (argc != 2) {
    fclose(pointsf);
  }

  /* set number of points */
  numxyn[0] = count;
  numxyn[1] = count;
  numxyn[2] = 1;

  /* open drr file */
  f = DRRopen(argv[1], 'r');
  DRRrdHead(f, &hdr);
  printf("hdr read ok\n");

  /* read in the required points */
  if( DRRReadXYN(f, &hdr, xpts, ypts, &N, numxyn, &values, &ptsread, &valid) != 0 ) {
    exit( 1 );
  } else {

    /* close file */
    DRRclose(f);

    printf("%d points read\n",ptsread);

    /* declare some pointers for looping over returned values */
    ras = (char *)values;
    uRas = (unsigned char*)ras;
    fRas = (float *)ras;
    iRas = (int *)ras;
    sRas = (short int *)ras;

    /* loop over returned values */
    printf("value valid\n");
    for( count = 0; count < ptsread; count++ )  {
        if (valid[count]) {

            switch (hdr.datatype) {
                case DRR_UBYTE:
                    printf("%d, %d\n", uRas[count], valid[count]);
                    break;
                case DRR_BYTE:
                    printf("%d, %d\n", ras[count], valid[count]);
                    break;
                case DRR_SHORT:
                    printf("%d, %d\n", sRas[count], valid[count]);
                    break;
                case DRR_LONG:
                    printf("%d, %d\n", iRas[count], valid[count]);
                    break;
                case DRR_FLOAT:
                    printf("%5.2f, %d\n", fRas[count], valid[count]);
                    break;
                  }

      } else {
        printf("NA, %d\n", valid[count]);
      }
    }
  }

  DRRFree(values);
  DRRFree(valid);
  return 0;
}
