/*****************************************

 * test routine for libdrr

 call as:
 ./test <in.drr> <out.drr>

 test reads in the in.drr, and writes to out.drr
 
 Both files should be identical
  
*****************************************/

#include <stdio.h>

#include "drr.h"

main(int argc, char *argv[])
{
    int i, npix;
    char rasterName[80];
    void *rast = NULL;
    FILE *f, *outf;
    DRRhdr hdr, outhdr;

    if (argc <3) {
        fprintf(stderr, "Usage: test.exe infile outfile\n");
        exit(1);
    }

 

    /* Read in the input file. */
    f = DRRopen(argv[1], 'r');
    DRRrdHead(f, &hdr);
    outhdr = hdr;

    /* Open the output file. */
    outf = DRRopen(argv[2], 'w');
    DRRwrHead(outf, &outhdr);

    /* Allocate arrays. */
    npix = hdr.nrows * hdr.ncols;
    rast = (void *)calloc(npix, sizeof(float)); /* allocate enough for the biggest datatype. */


    /* Accumulate all the infile onto the sum. */
    for (i=1; i<=hdr.Nrasters; i++) {
        if (hdr.rastersNamed) {
            DRRrdRasterName(i, f, &hdr, rasterName);
        }
        DRRrdRaster(i, f, &hdr, rast, hdr.ncols);

        if (outhdr.rastersNamed) {
            DRRwrNamedRaster(outf, &outhdr, rast, outhdr.ncols, rasterName);
        } else {
            DRRwrRaster(outf, &outhdr, rast, outhdr.ncols);
        }
    }

    DRRclose(f);
    DRRclose(outf);
}
