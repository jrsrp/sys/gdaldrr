/* Function prototypes for the generic run-length encoding routines. */

#ifdef __cplusplus
extern "C" {
#endif




void drrCompress(char *data, int nvals, char *compressedData, 
     int elementSize, int *compressedSize);

void drrUncompress(char *data, int nvals, char *compressedData, 
     int elementSize, int *compressedSize);



#ifdef __cplusplus
}
#endif

