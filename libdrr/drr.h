/* Definition of the structures used by the DRR routines.
   Neil Flood. 25 August 1992.

   Modified 19 Jan 1994. Changed all the short int stuff to int, and
   generally simplified the header. 
     
     2003-09-09 DPR add byteSwap
     2004-07-12 DPR typeo: chage DRRsetDflt to DRRsetDfltHdr
     2005-01-01 DPR merge alpha/PC/SGI versions.
*/

#ifndef DRR_H
#define DRR_H

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif


/* the image offsets (byte offsets of the compressed rasters)
   are stored in the drr file as a 4 byte int. If we ever
   want to compile drr for a machine where int is not 4byte
   you'll need to ifdef this */
typedef int int4byte;


/* Different projection types. */
#define LAT_LONG_PROJ 0
#define LCC_PROJ 1
#define RAW_IMAGERY_PROJ 2

/* Different types of default header known. */
#define DRR_QLD_LL 0
#define DRR_QLD_LCC 1
#define DRR_AUS_LL 2
#define DRR_AUS_LCC 3

/* Different data types. */
#define DRR_UBYTE 0
#define DRR_BYTE 1
#define DRR_SHORT 2
#define DRR_LONG 3
#define DRR_FLOAT 4

/* The sizes given here are the sizes of the data elements in the file, rather
   than the sizes of the similar elements in memory on the particular machine
   in question. */
#define SIZE_BYTE       1
#define SIZE_SHORT      2
#define SIZE_INT        4
#define SIZE_LONG       4
#define SIZE_IEEE_FLOAT 4



/* A few simple maximum sizes for the header struct. */
#define NUM_TEXT_LINES 3
#define TEXT_LINE_LEN 80
#define DATUMNAME_LEN 20

#define RASTERNAME_LEN 40

/* This definition is used as an array bound. It has a corresponding
   defined value in the FORTRAN stubs, which MUST be exactly the same.
*/
#define MAXIMUM_ALLOWED_RASTERS 2000


/* This is the structure used to store the header of a DRR file. */
typedef struct {
    int vmajor;
    int vminor;
    int nrows;
    int ncols;
    int datatype;
    int projection;
    float bottom;
    float left;
    float top;
    float right;
    int Nrasters;
    int maxNrasters;
    int linesPerTile;
    int rastersNamed;
    union {
    struct {
        long firstParallel;
        long secondParallel;
        long centralMeridian;
        long originLatitude;
        long falseEasting;
        long falseNorthing;
        float ae;
        float e2;
            char datumName[DATUMNAME_LEN+1];
    } lcc;
    } projParam;
    char textLine[NUM_TEXT_LINES][TEXT_LINE_LEN];
    int4byte imgOffsets[MAXIMUM_ALLOWED_RASTERS];
} DRRhdr;



FILE *DRRopen(char filename[], char access);

int DRRrdHead(FILE *f, DRRhdr *hdr);

int DRRwrHead(FILE *f, DRRhdr *hdr);


int DRRmodifyHead(FILE *f, DRRhdr *hdr);

int DRRrdRaster(int rasterNum, FILE *f, DRRhdr *hdr, char *raster, int arrayNcols);

int DRRReadXYN(FILE  *filePtr, const DRRhdr *hdr, const double *X, const double *Y, const int *N,
           const int numXYN[], char **xyValues, int *numValues, int **valuesValid);

int DRRscoopRaster(int rasterNum, FILE *f, DRRhdr *hdr, char *raster, int *nbytes);

int DRRwrRaster(FILE *f, DRRhdr *hdr, char *raster, int arrayNcols);

int DRRwrNamedRaster(FILE *f,DRRhdr *hdr,char *raster,int arrayNcols,char name[]);

int DRRgetRasterNum(FILE *f, DRRhdr *hdr, char *name);

int DRRunScoopRaster(FILE *f,DRRhdr *hdr,char *raster,int nbytes);

void DRRclose(FILE *f);

int DRRrdLine(int rasterNum,int lineNum,FILE *f,DRRhdr *hdr,char *line);

int DRRrdBlock(int rasterNum,int strtLine,int endLine,int strtCol,int endCol,
    FILE *f,DRRhdr *hdr,char *block,int arrayNcols);

int DRRReadTile(FILE  *f, const DRRhdr *hdr, int tileNum, int rasterNum, char **tileData);

void DRRsetDfltHdr(DRRhdr *hdr,int hdrType);

int DRRrdRasterName(int rasterNum,FILE *f,DRRhdr *hdr,char name[]);

float DRRpix2float(void *raster,DRRhdr *hdr,int pix);

void DRRfloat2pix(void *raster,DRRhdr *hdr,int pix,float val);

void DRRxy2rc(DRRhdr *hdr,double x,double y,int *r, int *c);

void DRRrc2xy(DRRhdr *hdr,float *x,float *y,int r, int c);

void DRRFree(void *ptr);


#ifdef __cplusplus
}
#endif

#endif
