! Structure definitions for the header of a DRR file. This is the FORTRAN
! structure definition, and mimics the corresponding C definition. The 
! same structure is used in both languages, without conversion, although
! this relies on the fact that both compilers lay out structures in the same
! way. As it happens they do, so we just use this fact. However, it could be
! a source of portability problems later on.
!
! Neil Flood. 15 Sep 1992.
!
! Modified so that there are only integers (and reals) no integer*2 etc.
! Neil Flood. 19 Jan 1994.
! Include Steve's fixes for ladel034/HPC for DRR_*_KIND. August 2009. DB.

module drr
    integer, parameter :: MAXIMUM_ALLOWED_RASTERS = 2000
    integer, parameter :: NUM_TEXT_LINES = 3, TEXT_LINE_LEN = 80
    integer, parameter :: DATUMNAME_LEN = 20

!   Symbolic names for some of the values in the header
    integer, parameter :: LAT_LONG_PROJ=0, LCC_PROJ=1
    integer, parameter :: RAW_IMAGERY_PROJ = 2
    integer, parameter :: DRR_QLD_LL=0, DRR_QLD_LCC=1
    integer, parameter :: DRR_UBYTE=0, DRR_BYTE=1, DRR_SHORT=2, DRR_LONG=3
    integer, parameter :: DRR_FLOAT=4
    
    ! To get the size of a C long and a C pointer. 
    ! Needs to be changed, depending on whether we are compiling 
    ! for 32 or 64 bit. Sorry. Use 9 for 32-bit, and 18 for 64-bit,
    ! becsuae 10**9 is roughly 2**32, and so on.
    integer, parameter :: C_LONG_KIND = selected_int_kind(18)
    integer, parameter :: C_PTR_KIND = selected_int_kind(18)

    integer, parameter :: DRR_FLOAT_KIND = selected_real_kind(5,10)
    integer, parameter :: DRR_LONG_KIND  = selected_int_kind(9)
    integer, parameter :: DRR_SHORT_KIND = selected_int_kind(4)
    integer, parameter :: DRR_BYTE_KIND  = selected_int_kind(2)
    integer, parameter :: DRR_UBYTE_KIND = selected_int_kind(2)


    type DRRhdr 
        SEQUENCE !  Stops compilers reordering the contents of this structure.
        integer vmajor
        integer vminor
        integer nrows
        integer ncols
        integer datatype
        integer projection
        real bottom
        real left
        real top
        real right
        integer Nrasters
        integer maxNrasters
        integer linesPerTile
        integer rastersNamed

        integer(kind=C_LONG_KIND) LCCfirstParallel
        integer(kind=C_LONG_KIND) LCCsecondParallel
        integer(kind=C_LONG_KIND) LCCcentralMeridian
        integer(kind=C_LONG_KIND) LCCoriginLatitude
        integer(kind=C_LONG_KIND) LCCfalseEasting
        integer(kind=C_LONG_KIND) LCCfalseNorthing
        real LCCae
        real LCCe2
!       This string length should be DATUMNAME_LEN+1, but
!       the SGI compiler won't let me.
        character*21 LCCdatumName

!       The following is some padding to make the FORTRAN and C versions 
!       this structure match. The amount of padding required is known
!       simply empirically i.e. this much makes it all line up.
        ! Now compiling for SGI-64, no longer needed.
        character padding(3)

!       This string length should be TEXT_LINE_LEN, but
!       the SGI compiler won't let me.
        character*80 textLine(NUM_TEXT_LINES)
        integer imgOffsets(MAXIMUM_ALLOWED_RASTERS)
    end type


!   Declare the names of the functions, so that when using 
!   no implicit typing we don't get "undeclared function" messages.
    integer(kind=C_PTR_KIND) DRRopen
    integer DRRrdHead, DRRrdRaster, DRRwrHead, DRRwrRaster
    integer DRRrdLine, DRRrdBlock, DRRrdRasterName, DRRwrNamedRaster
    integer DRRscoopRaster, DRRunscoopRaster, DRRmodifyHead
    integer DRRgetRasterNum

    external DRRclose
    external DRRopen, DRRrdHead, DRRrdRaster, DRRwrHead, DRRwrRaster
    external DRRrdLine, DRRrdBlock, DRRrdRasterName, DRRwrNamedRaster
    external DRRscoopRaster, DRRunscoopRaster, DRRmodifyHead
    external DRRgetRasterNum
end module
