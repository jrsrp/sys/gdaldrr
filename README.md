DRR and libdrr
===============


To get a gdal driver for gdal, you first need to make the libdrr.so library.
Once you have that, you can make the gdal drr driver.

I've gathered the code for both these and placed them in this repository.

It is a little unclear where these should come from, I copied the libdrr files
from /opt/sw/fwsrc/rsc/drr/, since I couldn't find a better location.

Check the contents of `the drr-1.0.BuildInstructions`, the comments of which include
the following:

```Makefile
# The DRR library was created by the pasture modelling group, now under QCCCE,
# and is notionally maintained by them. However, since they seem to want to
# build it with different compilers and so on, I have taken the decision
# to maintain our own build of it. The source code is still taken from their
# SVN repository. There has only ever been one release of it (1.0), but
# conceivably there could be another one.

# Anyway, we merely maintain our own Makefile for it, rather than the code itslf.
# Our Makefile is stored in our Mercurial repository.

# First, check out the code from the SVN repository. At the time I wrote these
# instructions, the last log message was by Andrej, on 2010-06-21, so if there is
# something more recent than that, you had better find out what, and why.
module load svn
svn co svn://sisvn/CINRS/dres/drr/libdrr ./libdrr
cd libdrr
```

The driver file `DRRdataset.cpp` (in directory DRR) were originally held in `rscrepo/rsc/img/gdal/drivers/DRR`
but also in `rscrepo/rsc/img/drr/DRRdataset.cpp` and in `rsc/img/gdal/arcdrr/DRRdataset.cpp`.
The first two locations seem to be identical. The one in `arcdrr` differs only that it
includes the line

```c
#include "stdafx.h"
```

It seems this version was built by Sam Gillingham in March 2008 (or he at least added the
files to the repo). See that directory if you need an esri driver.

## Compiling and Usage

First compile the `libdrr` library:

```
cd libdrr
make -f Makefile.gfortran
cd ..
```

Now compile the driver:

```
cd DRR
export GDAL_INCLUDE_PATH=/usr/local/include
export GDAL_LIB_PATH="/usr/local/lib -lgdal"
export DRR_INCLUDE_PATH=$(realpath ../libdrr)
export DRR_LIB_PATH=$(realpath ../libdrr)
SHEXT=.so CPPCOMP=g++ make -f Makefile.nocheck 
```

Copy the shared object to the gdal plugin directory, and
make the `libdrr.so` object available

```sh
PLUGIN_DIR=$(gdal-config --prefix)/lib/gdalplugins
cp gdal_DRR.so $PLUGIN_DIR
cp ../libdrr/libdrr.so /usr/local/lib/
ldconfig
```

If everything worked, you should see DRR as a format:

```sh
gdalinfo --formats|grep -i drr
```

gives

```sh
DRR -raster- (rw): Qld. Dept. of Natural Resources, Mines and Water DRR (.drr)
```

# Test

There is a test raster provided

```sh
cd test
gdalinfo 202301.morain.drr 
```

Should produce

```sh
Driver: DRR/Qld. Dept. of Natural Resources, Mines and Water DRR (.drr)
Files: 202301.morain.drr
Size is 841, 681
Coordinate System is:
GEOGCRS["WGS 84",
    ENSEMBLE["World Geodetic System 1984 ensemble",
        MEMBER["World Geodetic System 1984 (Transit)"],
        MEMBER["World Geodetic System 1984 (G730)"],
        MEMBER["World Geodetic System 1984 (G873)"],
        MEMBER["World Geodetic System 1984 (G1150)"],
        MEMBER["World Geodetic System 1984 (G1674)"],
        MEMBER["World Geodetic System 1984 (G1762)"],
        MEMBER["World Geodetic System 1984 (G2139)"],
        ELLIPSOID["WGS 84",6378137,298.257223563,
            LENGTHUNIT["metre",1]],
        ENSEMBLEACCURACY[2.0]],
    PRIMEM["Greenwich",0,
        ANGLEUNIT["degree",0.0174532925199433]],
    CS[ellipsoidal,2],
        AXIS["geodetic latitude (Lat)",north,
            ORDER[1],
            ANGLEUNIT["degree",0.0174532925199433]],
        AXIS["geodetic longitude (Lon)",east,
            ORDER[2],
            ANGLEUNIT["degree",0.0174532925199433]],
    USAGE[
        SCOPE["Horizontal component of 3D system."],
        AREA["World."],
        BBOX[-90,-180,90,180]],
    ID["EPSG",4326]]
Data axis to CRS axis mapping: 2,1
Origin = (111.974999999999994,-9.975000000000000)
Pixel Size = (0.050000000000000,-0.050000000000000)
Metadata:
  DRRINFO_1=~RRunknown~ELmr~ALspline.1~TYdaily~SL0.1~SX0.0~UNmm~MV-1.0~CQ00000000~CD2023021
  DRRINFO_2=32156~BE20230213~SQ00000000~BRMonthly rainfall interpolated from bulk update 20
  DRRINFO_3=22 + nightly feeds
Corner Coordinates:
Upper Left  ( 111.9750000,  -9.9750000) (111d58'30.00"E,  9d58'30.00"S)
Lower Left  ( 111.9750000, -44.0250000) (111d58'30.00"E, 44d 1'30.00"S)
Upper Right ( 154.0250000,  -9.9750000) (154d 1'30.00"E,  9d58'30.00"S)
Lower Right ( 154.0250000, -44.0250000) (154d 1'30.00"E, 44d 1'30.00"S)
Center      ( 133.0000000, -27.0000000) (133d 0' 0.00"E, 27d 0' 0.00"S)
Band 1 Block=841x128 Type=Int16, ColorInterp=Undefined
  Offset: 0,   Scale:0.1
```

Test info:

```
DISTRIB_DESCRIPTION="Ubuntu 22.04.1 LTS"
gdal 3.5.2, 3.6.1

```

You can use our docker image to test:

```sh
docker run --rm -it  qldrsc/jrsrp_base:drr gdalinfo 202301.morain.drr
```

Or in singularity:

```sh
singularity exec docker://qldrsc/jrsrp_base:drr gdalinfo 202301.morain.drr
```
