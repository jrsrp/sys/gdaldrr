/******************************************************************************
 *
 * Module:  DRR Reader
 *          - adapted from jdemdataset.cpp
 *
 */

#include <stdio.h>
#include <new>

#include "gdal_pam.h"
#include "drr.h"

#define PRINT_DEBUG_INFO 0

// Prototypes 
static void dump_raster(void *raster, DRRhdr *hdr, int xskip, int yskip);
static bool read_DRR_scale_factors(DRRhdr *hdr, double& scale, double& offset);

#include <new>
using std::bad_alloc;

/************************************************************************/
/*                  Function reads in a DRR Header                      */
/************************************************************************/

void read_drr_header(FILE *fp, DRRhdr *hdr, char *filename)
{
   // Read the DRR header
   if ( DRRrdHead(fp, hdr) != 0 ) {
      fprintf(stderr, "Error reading DRR file header for file: %s\n", filename);
      exit(1);
   }
}

/************************************************************************/
/*                  Function outputs the contents of a raster           */
/************************************************************************/

void dump_raster(void *raster, DRRhdr *hdr, int xskip, int yskip)
{
   int i, j, k;

   switch (hdr->datatype) {
      case 0: // Unsigned byte
      {
         unsigned char *image = static_cast<unsigned char *>(raster);
         for (i=k=0; i<hdr->nrows; i+=yskip) {
            k = i * hdr->ncols;
            for (j=0; j<hdr->ncols; j+=xskip, k+=xskip) {
               fprintf(stderr, "%4d %4d %hu\n", i, j, (unsigned short int)image[k]);
            }
            fprintf(stderr, "\n");
         }
         break;
      }
      case 1: // Signed byte
      {
         char *image = static_cast<char *>(raster);
         for (i=k=0; i<hdr->nrows; i+=yskip) {
            k = i * hdr->ncols;
            for (j=0; j<hdr->ncols; j+=xskip, k+=xskip) {
               fprintf(stderr, "%4d %4d %hd\n", i, j, (short int)image[k]);
            }
            fprintf(stderr, "\n");
         }
         break;
      }
      case 2: // Short int
      {
         short int *image = static_cast<short int *>(raster);
         for (i=k=0; i<hdr->nrows; i+=yskip) {
            k = i * hdr->ncols;
            for (j=0; j<hdr->ncols; j+=xskip, k+=xskip) {
               fprintf(stderr, "%4d %4d %hd\n", i, j, image[k]);
            }
            fprintf(stderr, "\n");
         }
         break;
      }
      case 3: // Long int
      {
         int *image = static_cast<int *>(raster);
         for (i=k=0; i<hdr->nrows; i+=yskip) {
            k = i * hdr->ncols;
            for (j=0; j<hdr->ncols; j+=xskip, k+=xskip) {
               fprintf(stderr, "%4d %4d %d\n", i, j, image[k]);
            }
            fprintf(stderr, "\n");
         }
         break;
      }
      case 4: // Float
      {
         float *image = static_cast<float *>(raster);
         for (i=k=0; i<hdr->nrows; i+=yskip) {
            k = i * hdr->ncols;
            for (j=0; j<hdr->ncols; j+=xskip, k+=xskip) {
               fprintf(stderr, "%4d %4d %f\n", i, j, image[k]);
            }
            fprintf(stderr, "\n");
         }
         break;
      }
   }
}

/************************************************************************/
/*                            DRRRasterBand                             */
/************************************************************************/

class DRRDataset;

class DRRRasterBand : public GDALPamRasterBand
{
    friend class DRRDataset;

    double dfOffset;
    double dfScale;

    public:
        // stores a copy of the header from the dataset
        DRRhdr hdr;

        DRRRasterBand(DRRDataset *, int);
        DRRRasterBand(DRRDataset *, int, DRRhdr *);
    
        virtual CPLErr IReadBlock( int, int, void * );
        virtual double GetOffset( int *pbSuccess = NULL );
        virtual CPLErr SetOffset( double dfNewValue );
        virtual double GetScale( int *pbSuccess = NULL );
        virtual CPLErr SetScale( double dfNewValue );
};

/************************************************************************/
/*				DRRDataset				*/
/************************************************************************/

class DRRDataset final: public GDALPamDataset
{
    friend class  DRRRasterBand;

    DRRhdr hdr;
    FILE         *fp;
    GByte         pabyHeader[512];
    OGRSpatialReference m_oSRS{};

  public:
    ~DRRDataset();
    DRRDataset();
    static GDALDataset* Open( GDALOpenInfo * );
    const char *_GetProjectionRef();
    CPLErr 	   GetGeoTransform( double * padfTransform );
    const OGRSpatialReference* GetSpatialRef() const override;

};

/************************************************************************/
/*                           DRRRasterBand()                            */
/************************************************************************/

DRRRasterBand::DRRRasterBand( DRRDataset *poDS, int nBand )

{
    #if PRINT_DEBUG_INFO
        fprintf(stderr, "In two-arg DRRRasterBand::DRRRasterBand()\n");
    #endif

    this->poDS  = poDS;
    this->nBand = nBand;
    
    eDataType   = GDT_Byte;

    // Set the block size = raster size
    nRasterXSize = poDS->GetRasterXSize();
    nRasterYSize = poDS->GetRasterYSize();
    nBlockXSize  = poDS->GetRasterXSize();
    nBlockYSize  = poDS->GetRasterYSize();
    nBlocksPerRow    = 1;
    nBlocksPerColumn = 1;

    // Set some defaults
    eDataType = GDT_Byte;

    double scale  = 1.0;
    double offset = 0.0;
    SetScale(scale);    
    SetOffset(offset);
}

DRRRasterBand::DRRRasterBand( DRRDataset *poDS, int nBand, DRRhdr *phdr )
{
   #if PRINT_DEBUG_INFO
       fprintf(stderr, "In three-arg DRRRasterBand::DRRRasterBand()\n");
   #endif

   // Store a copy of the DRR header
   memcpy(&hdr, phdr, sizeof(DRRhdr));

   this->poDS  = poDS;
   this->nBand = nBand;
   
   // Set the datatype
   switch (hdr.datatype)
      {
      case 0: // Unsigned byte
              eDataType = GDT_Byte;
              break;
      case 1: // Signed byte
              eDataType = GDT_Byte;
              break;
      case 2: // Short int
              eDataType = GDT_Int16;
              break;
      case 3: // Long int
              eDataType = GDT_Int32;
              break;
      case 4: // Float
              eDataType = GDT_Float32;
              break;
    }

    // DRR tile sizes are blocks having:
    // width  = full width of the raster,
    // height = "hdr.linesPerTile" lines 

    if ( hdr.linesPerTile > 0  &&  hdr.linesPerTile <= hdr.nrows ) {
       nBlockXSize = hdr.ncols;
       nBlockYSize = hdr.linesPerTile;
    }
    else {
       // Set the block size = raster size
       nBlockXSize = hdr.ncols;
       nBlockYSize = hdr.nrows;
    }

    // Set the scale and offset (if this info is available)
    // NOTE: Actual value = (raster value * scale) + offset
    double scale  = 1.0; // Default value
    double offset = 0.0; // Default value 
    read_DRR_scale_factors(phdr, scale, offset);
    SetScale(scale);    
    SetOffset(offset);

    #if PRINT_DEBUG_INFO
        fprintf(stderr, "Blocksize: %d x %d, band no.: %d\n", nBlockXSize, nBlockYSize, nBand);
    #endif
}

/************************************************************************/
/*                             GetOffset()                              */
/************************************************************************/

double DRRRasterBand::GetOffset( int *pbSuccess )
{
    if( pbSuccess != NULL )
        *pbSuccess = TRUE;

    return dfOffset;
}

/************************************************************************/
/*                             SetOffset()                              */
/************************************************************************/

CPLErr DRRRasterBand::SetOffset( double dfNewOffset )
{
    dfOffset = dfNewOffset;
    return CE_None;
}

/************************************************************************/
/*                              GetScale()                              */
/************************************************************************/

double DRRRasterBand::GetScale( int *pbSuccess )
{
    if( pbSuccess != NULL )
        *pbSuccess = TRUE;

    return dfScale;
}

/************************************************************************/
/*                              SetScale()                              */
/************************************************************************/

CPLErr DRRRasterBand::SetScale( double dfNewScale )
{
    dfScale = dfNewScale;
    return CE_None;
}

/************************************************************************/
/* Function attempts to extract the scale and offset factors that may   */
/* be stored in the DRR metadata strings in the format:                 */
/*                                                                      */
/* ~SL0.5~SX-40.0                                                       */
/*                                                                      */
/************************************************************************/

bool read_DRR_scale_factors(DRRhdr *hdr, double& scale, double& offset)
{
    int   i, length;
    bool  scale_found  = false;
    bool  offset_found = false;
    char *start = NULL;
    char *end   = NULL;
    char  buffer[TEXT_LINE_LEN];

    // Attempt to find the scale
    for (i=0; i<NUM_TEXT_LINES;i++) {
       if ( (start = strstr(hdr->textLine[i], "~SL")) ) {
          start += 3;

          // Attempt to find the end of the scale factor
          if ( (end = strstr(start, "~")) )  
             --end;
          else {
             // Assume the scale factor is the last entry in this row of metadata
             end = hdr->textLine[i] + TEXT_LINE_LEN-1;
             if ( *end == 0 ) // A terminating NUL may be present
                --end;
          }
          // Attempt to read the scale factor
          length = (int)(end-start)+1;
          memcpy(buffer, start, length);
          buffer[length] = 0; // Add the terminating NUL

          if ( sscanf(buffer, "%lf", &scale) == 1 ) {
             scale_found = true;
             #if PRINT_DEBUG_INFO
                 fprintf(stderr, "Scale: %f\n", scale);
             #endif
             break;
          }
       }
    } // Loop over rows of DRR metadata

    // Attempt to find the offset
    for (i=0; i<NUM_TEXT_LINES;i++) {
       if ( (start = strstr(hdr->textLine[i], "~SX")) ) {
          start += 3;

          // Attempt to find the end of the offset factor
          if ( (end = strstr(start, "~")) )
             --end;
          else {
             // Assume the offset factor is the last entry in this row of metadata
             end = hdr->textLine[i] + TEXT_LINE_LEN-1;
             if ( *end == 0 ) // A terminating NUL may be present
                --end;
          }
          // Attempt to read the offset factor
          length = (int)(end-start)+1;
          memcpy(buffer, start, length);
          buffer[length] = 0; // Add the terminating NUL

          if ( sscanf(buffer, "%lf", &offset) == 1 ) {
             offset_found = true;
             #if PRINT_DEBUG_INFO
                 fprintf(stderr, "Offset: %f\n", offset);
             #endif
             break;
          }
       }
    } // Loop over rows of DRR metadata

    return (scale_found && offset_found) ? true : false;
}

/************************************************************************/
/*                             IReadBlock()                             */
/*                                                                      */
/* If the "hdr.linesPerTile" value was set in the DRR header, then the  */
/* block size = hdr.linesPerTile rows of the raster, otherwise:         */
/* block size = raster size, so reading a block reads the entire raster */
/************************************************************************/

CPLErr DRRRasterBand::IReadBlock( int nBlockXOff, int nBlockYOff, void * pImage )
{
    int   line, line_start, line_end, shift;
    char *ptr;

    #if PRINT_DEBUG_INFO
        fprintf(stderr, "In DRRRasterBand::IReadBlock\n");
        fprintf(stderr, "- block offsets: %d x %d, into band: %d\n", nBlockXOff, nBlockYOff, nBand);
    #endif

    // DRRs blocks extend across the full width of the raster, so there is only one block
    // in the x-direction
    if ( nBlockXOff != 0 ) {
        fprintf(stderr, "Error: DRRRasterBand::IReadBlock attempting to read unavailable block\n");
        return CE_Failure;
    }

    DRRDataset *DRR_poDS = (DRRDataset *)poDS;

    // Attempt to read in the specified band of the DRR raster
    if ( nBlockYSize == nRasterYSize ) {
       #if PRINT_DEBUG_INFO
           fprintf(stderr, "Reading entire raster (blocksize: %d, raster size: %d)\n",
           nBlockYSize, nRasterYSize);
       #endif
       // Read the entire raster
       if ( DRRrdRaster(nBand, DRR_poDS->fp, &hdr, (char *)pImage, hdr.ncols) != 0 ) {
          fprintf(stderr, "Error reading DRR file\n");
          return CE_Failure;
       }
    }
    else {
       #if PRINT_DEBUG_INFO
           fprintf(stderr, "Reading single tile of raster (blocksize: %d, raster size: %d)\n",
           nBlockYSize, nRasterYSize);
       #endif
       // Read just the specified block
       line_start = nBlockYOff * hdr.linesPerTile + 1;
       line_end   = line_start + hdr.linesPerTile - 1;
       if ( line_end > hdr.nrows ) 
          line_end = hdr.nrows;
       ptr = (char *)pImage;
       switch (hdr.datatype) {
          case 0: // Unsigned byte
             shift = hdr.ncols * sizeof(unsigned char);
             break;
          case 1: // Signed byte
             shift = hdr.ncols * sizeof(char);
             break;
          case 2: // Short int
             shift = hdr.ncols * sizeof(short int);
             break;
          case 3: // Long int
             shift = hdr.ncols * sizeof(int);
             break;
          case 4: // Float
             shift = hdr.ncols * sizeof(float);
             break;
          default:
            fprintf(stderr, "Unknown data type %d\n", hdr.datatype);
            return CE_Failure;
       }
       for (line=line_start; line<=line_end; line++, ptr+=shift) {
          #if PRINT_DEBUG_INFO
              fprintf(stderr, "Reading line: %d in DRR file\n", line);
          #endif
          if ( DRRrdLine(nBand, line, DRR_poDS->fp, &hdr, ptr) != 0 ) {
             fprintf(stderr, "Error reading line %d in DRR file\n", line);
             return CE_Failure;
          }
       } // Loop over all lines in the tile
    }

    #if PRINT_DEBUG_INFO
        // Output the raster in ASCII format
        fprintf(stderr, "Raster band %d read in DRRRasterBand::IReadBlock()\n", nBand);
        //dump_raster((void*)pImage, &hdr, 10, 10);
    #endif
    
    return CE_None;
}

/************************************************************************/
/*				DRRDataset				*/
/************************************************************************/


DRRDataset::DRRDataset()
{
    std::fill_n(pabyHeader, CPL_ARRAYSIZE(pabyHeader), static_cast<GByte>(0));

    m_oSRS.SetAxisMappingStrategy(OAMS_TRADITIONAL_GIS_ORDER);
    // we'll try resetting the proj later
    m_oSRS.importFromEPSG(4326);  // what is this supposed to be?


}

/************************************************************************/
/*                            ~DRRDataset()                             */
/************************************************************************/

DRRDataset::~DRRDataset()

{
   #if ( (GDAL_VERSION_MAJOR >= 3 && GDAL_VERSION_MINOR >= 5) || GDAL_VERSION_MAJOR >=4 )
      // the signature of FlushCache changed with GDAL 3.5 (See https://github.com/OSGeo/gdal/pull/4653/commits/4b46f534fed80d31c3e15c1517169f40694a4a3e#)
      FlushCache(true);
   #else
      FlushCache();  
   #endif

   if( fp != NULL )
   {
      // this call also seems to close the file handle
      DRRclose( fp );
   }
}

/************************************************************************/
/*                                Open()                                */
/************************************************************************/

GDALDataset *DRRDataset::Open( GDALOpenInfo * poOpenInfo )

{
/* -------------------------------------------------------------------- */
/*      Before trying DRROpen() we first verify that there is at        */
/*      least one "\n#keyword" type signature in the first chunk of     */
/*      the file.                                                       */
/* -------------------------------------------------------------------- */

    #if PRINT_DEBUG_INFO
        fprintf(stderr, "In DRRDataset::Open\n");
    #endif



#if (GDAL_VERSION_MAJOR >= 2)
    // In GDAL 2.x
    // pOpenInfo doesn't have a 'fp' field - has a 'fpL' field
    // instead which is of type VSILFILE which can't be passed to the
    // DRR routines
    if( poOpenInfo->nHeaderBytes < 50 )
        return NULL;
#else
    if( poOpenInfo->fp == NULL || poOpenInfo->nHeaderBytes < 50 )
        return NULL;
#endif

    /* Check if the first line of the header contains: "DRES RASTER V" */
    if( !EQUALN((char *)poOpenInfo->pabyHeader,"DRES RASTER V",13) ) {
        #if PRINT_DEBUG_INFO
            fprintf(stderr, "File is NOT a DRR\n");
        #endif
        return NULL;
    }
    #if PRINT_DEBUG_INFO
    else
        fprintf(stderr, "File is a DRR\n");
    #endif
    
/* -------------------------------------------------------------------- */
/*      Create a corresponding GDALDataset.                             */
/* -------------------------------------------------------------------- */
    DRRDataset 	*poDS;
    poDS = new DRRDataset();

#if (GDAL_VERSION_MAJOR >= 2)
    // We can't use the file handle given
    // (see note above) so open it with VSIFOpen()
    poDS->fp = VSIFOpen(poOpenInfo->pszFilename, "rb");
    if( poDS->fp == NULL )
    {
        delete poDS;
        return NULL;
    }
#else
    poDS->fp = poOpenInfo->fp;
    poOpenInfo->fp = NULL;
#endif
    
/* -------------------------------------------------------------------- */
/*      Read the header.                                                */
/* -------------------------------------------------------------------- */
    VSIFSeek( poDS->fp, 0, SEEK_SET );
    read_drr_header(poDS->fp, &poDS->hdr, poOpenInfo->pszFilename);
    /* set the proj information here */
    if ( poDS->hdr.projection == 0 ) {
      poDS->m_oSRS.importFromEPSG(4326);  // what is this supposed to be?
    } else {
      fprintf(stderr, "GetProjectionRef for Lambert Conformal Conic projection is not implemented yet\n");
    }
    poDS->nRasterXSize = (int)poDS->hdr.ncols;
    poDS->nRasterYSize = (int)poDS->hdr.nrows;

/* -------------------------------------------------------------------- */
/*      Create band information objects.                                */
/*                                                                      */
/*   DRRs can contain multiple rasters                                  */
/*   - each raster is loaded as a separate band                         */
/*   DRRs are tiled into blocks                                         */
/*   - block reading is not implemented                                 */
/* -------------------------------------------------------------------- */
    poDS->nBands = poDS->hdr.Nrasters;
    for (int i=1; i<=poDS->hdr.Nrasters; i++) {
        #if PRINT_DEBUG_INFO
            fprintf(stderr, "Loading band %d from raster: %d\n", i, poDS->nBands);
        #endif
        poDS->SetBand( i, new DRRRasterBand( poDS, i, &poDS->hdr ));
    }

/* -------------------------------------------------------------------- */
/*      Store the DRR metadata                                          */
/* -------------------------------------------------------------------- */
   CPLErr err;
    
   if ( strlen(poDS->hdr.textLine[0]) > 0 ) {
       err = poDS->SetMetadataItem("DRRINFO_1", poDS->hdr.textLine[0]);
       #if PRINT_DEBUG_INFO
            fprintf(stderr, "Stored metadata: %s = %s\n", "DRRINFO_1", poDS->hdr.textLine[0]);
       #endif
       if ( err != CE_None )
          fprintf(stderr, "Warning: could not copy DRR metadata to new file\n");
    }

   if ( strlen(poDS->hdr.textLine[1]) > 0 ) {
       err = poDS->SetMetadataItem("DRRINFO_2", poDS->hdr.textLine[1]);
       #if PRINT_DEBUG_INFO
            fprintf(stderr, "Stored metadata: %s = %s\n", "DRRINFO_2", poDS->hdr.textLine[1]);
       #endif
       if ( err != CE_None )
          fprintf(stderr, "Warning: could not copy DRR metadata to new file\n");
    }

   if ( strlen(poDS->hdr.textLine[2]) > 0 ) {
       err = poDS->SetMetadataItem("DRRINFO_3", poDS->hdr.textLine[2]);
       #if PRINT_DEBUG_INFO
            fprintf(stderr, "Stored metadata: %s = %s\n", "DRRINFO_3", poDS->hdr.textLine[2]);
       #endif
       if ( err != CE_None )
          fprintf(stderr, "Warning: could not copy DRR metadata to new file\n");
    }
/* -------------------------------------------------------------------- */
/*      Initialize any PAM information.                                 */
/* -------------------------------------------------------------------- */
    poDS->SetDescription( poOpenInfo->pszFilename );
    poDS->TryLoadXML();

    return( poDS );
}

/************************************************************************/
/*                           DRRCreateCopy()                            */
/************************************************************************/

static GDALDataset *
DRRCreateCopy(const char * pszFilename, GDALDataset *poSrcDS, 
               int bStrict, char ** papszOptions, 
               GDALProgressFunc pfnProgress, void * pProgressData )

{
    #if PRINT_DEBUG_INFO
        fprintf(stderr, "In DRRCreateCopy()\n");
    #endif

    int             i, elem_size, n_pixels, BS_X, BS_Y, block_size_y;
    bool            cast_to_signed;
    char           *data;
    DRRhdr          hdr;
    CPLErr          eErr;
    GDALDataType    eType;
    GDALRasterBand *band; 
    FILE           *drrfile;

    int  nBands      = poSrcDS->GetRasterCount();
    int  nXSize      = poSrcDS->GetRasterXSize();
    int  nYSize      = poSrcDS->GetRasterYSize();

    #if PRINT_DEBUG_INFO
        fprintf(stderr, "nBands: %d nXSize: %d nYSize: %d\n", nBands, nXSize, nYSize);
        fprintf(stderr, "Output file: %s\n", pszFilename);
    #endif

    // Check all bands have the same datatype (a DRR can only have one datatype)
    band = poSrcDS->GetRasterBand(1);
    eType = band->GetRasterDataType();
    for (i=2; i<=nBands; i++) {
       if ( poSrcDS->GetRasterBand(i)->GetRasterDataType() != eType ) {
          fprintf(stderr, "Error: cannot pack multiple rasters of varying datatype into a DRR\n");
          exit(1);
       }
    }

    // Check all bands have the same Y-blocksize (a DRR can only have one "linesPerTile" value)
    band = poSrcDS->GetRasterBand(1);
    band->GetBlockSize(&BS_X, &BS_Y);
    block_size_y = BS_Y;
    for (i=2; i<=nBands; i++) {
       poSrcDS->GetRasterBand(i)->GetBlockSize(&BS_X, &BS_Y);
       if ( BS_Y != block_size_y ) {
          fprintf(stderr, "Error: cannot pack multiple rasters of varying blocksize into a DRR\n");
          exit(1);
       }
    }

    if ( ! bStrict ) {
       fprintf(stderr, "Warning: there may be errors in mapping datatypes, bStrict=false\n");
    }

    // Determine the DRR datatype
    cast_to_signed = false;
    switch (eType) {
      case GDT_Byte:   // Unsigned byte
              hdr.datatype = 0;
              elem_size = sizeof(unsigned char);
              break;
      case GDT_UInt16: // Short int
              cast_to_signed = true;
      case GDT_Int16:  // Short int
              hdr.datatype = 2;
              elem_size = sizeof(short int);
              break;
      case GDT_Int32: // Long int
              hdr.datatype = 3;
              elem_size = sizeof(int);
              break;
      case GDT_Float32: // Float
              hdr.datatype = 4;
              elem_size = sizeof(float);
              break;
      default:
              fprintf(stderr, "Error: cannot cast GDAL datatype to a suitable DRR datatype\n");
              exit(1);
    }

    #if PRINT_DEBUG_INFO
        fprintf(stderr, "DRR datatype: %d\n", hdr.datatype);
    #endif

    // Get the transform matrix so we can compute the boundary of the DRR
    double transform_matrix[6], min_long, max_long, min_lat, max_lat;
    eErr = poSrcDS->GetGeoTransform(transform_matrix);
    if ( eErr != CE_Failure ) {

       // GDAL uses the top left corner of the pixel as the origin. DRR uses the pixel centre
       int P, L;
       double pixel_width  = transform_matrix[1];
       double pixel_height = transform_matrix[5];
   
       P = 0;      L = 0;
       min_long = transform_matrix[0] + P*transform_matrix[1] + L*transform_matrix[2];
       P = nXSize; L = 0;
       max_long = transform_matrix[0] + P*transform_matrix[1] + L*transform_matrix[2];
   
       P = 0;      L = nYSize;
       min_lat  = transform_matrix[3] + P*transform_matrix[4] + L*transform_matrix[5];
       P = 0;      L =      0;
       max_lat  = transform_matrix[3] + P*transform_matrix[4] + L*transform_matrix[5];
   
       min_long += pixel_width/2.0;
       max_long += pixel_width/2.0;
       min_lat  -= pixel_height/2.0;
       max_lat  -= pixel_height/2.0;

       // DRRs use the pixel centre as the origin. This requires us to adjust the
       // range in latitude and longitude, as:
       // max_long = min_long + (no_cols-1)*pixel_width
       // instead of: 
       // max_long = min_long + no_cols*pixel_width
       // and similarly for latitude
       max_long -= pixel_width;
       max_lat  += pixel_height;  // Sign is negative as the pixel height is -ve

       #if PRINT_DEBUG_INFO
           fprintf(stderr, "Imported transform: %15.10f, %15.10f, %15.10f\n",
           transform_matrix[0], transform_matrix[1], transform_matrix[2]);
           fprintf(stderr, "                    %15.10f, %15.10f, %15.10f\n",
           transform_matrix[3], transform_matrix[4], transform_matrix[5]);
           fprintf(stderr, "Pixel width: %f height: %f\n", pixel_width, pixel_height);
           fprintf(stderr, "Latitude min: %f max: %f\n", min_lat, max_lat);
           fprintf(stderr, "Longitude min: %f max: %f\n", min_long, max_long);
       #endif
    }
    else {
       // Transformation was not specified for the input class
       // - the DRR library reports an error if null values are used:
       // min_long = max_long = min_lat = max_lat = 0.0;
       // --> enter the default DRR Australian region
       min_lat  = -44.0;
       min_long = 112.0;
       max_lat  = -10.0;
       max_long = 154.0;
    }

    hdr.maxNrasters  = nBands;
    hdr.Nrasters     = 0;
    hdr.linesPerTile = block_size_y; // Block size
    hdr.nrows        = nYSize;
    hdr.ncols        = nXSize;
    hdr.projection   = LAT_LONG_PROJ;
    hdr.bottom       = static_cast<float>(min_lat);
    hdr.left         = static_cast<float>(min_long);
    hdr.top          = static_cast<float>(max_lat);
    hdr.right        = static_cast<float>(max_long);
    memset(hdr.textLine, 0, NUM_TEXT_LINES*TEXT_LINE_LEN);
    memset(hdr.imgOffsets, 0, MAXIMUM_ALLOWED_RASTERS*sizeof(int4byte));

    // Add any metadata that may be present
    char *metadata_1 = NULL;
    char *metadata_2 = NULL;
    char *metadata_3 = NULL;
    metadata_1 = (char *)poSrcDS->GetMetadataItem("DRRINFO_1");
    metadata_2 = (char *)poSrcDS->GetMetadataItem("DRRINFO_2");
    metadata_3 = (char *)poSrcDS->GetMetadataItem("DRRINFO_3");

    #if PRINT_DEBUG_INFO
        fprintf(stderr, "Inherited metadata: %s\n", metadata_1);
        fprintf(stderr, "Inherited metadata: %s\n", metadata_2);
        fprintf(stderr, "Inherited metadata: %s\n", metadata_3);
    #endif

    int md_line_no = 0;
    if ( metadata_1 ) {
       if ( strlen(metadata_1) >= TEXT_LINE_LEN )
          strncpy(hdr.textLine[md_line_no++], metadata_1, TEXT_LINE_LEN-1);
       else
          strcpy(hdr.textLine[md_line_no++], metadata_1);
    }
    if ( metadata_2 ) {
       if ( strlen(metadata_2) >= TEXT_LINE_LEN )
          strncpy(hdr.textLine[md_line_no++], metadata_2, TEXT_LINE_LEN-1);
       else
          strcpy(hdr.textLine[md_line_no++], metadata_2);
    }
    if ( metadata_3 ) {
       if ( strlen(metadata_3) >= TEXT_LINE_LEN )
          strncpy(hdr.textLine[md_line_no++], metadata_3, TEXT_LINE_LEN-1);
       else
          strcpy(hdr.textLine[md_line_no++], metadata_3);
    }

    if ( (drrfile = DRRopen((char *)(pszFilename), 'w')) == NULL ) {
       fprintf(stderr, "Error opening drr file - bye!\n");
       exit(1);
    }
    if ( DRRwrHead(drrfile, &hdr) != 0 ) {
       fprintf(stderr, "Error writing drr header - bye!\n");
       exit(1);
    }
 
    // Allocate memory to store the data
    n_pixels = hdr.ncols * hdr.nrows;
    try {
       #ifdef SGI
          err_line_no = __LINE__;
          err_filename = __FILE__;
       #endif
       data = new char[n_pixels * elem_size];
    }
    #ifdef SGI
       catch(...) { }
    #else
       catch(bad_alloc) {
          fprintf(stderr, "Error allocating memory at line %d in file %s - bye!\n",
          __LINE__, __FILE__);
          exit(1);
       }
    #endif

    for (i=1; i<=nBands; i++) {
        band = poSrcDS->GetRasterBand(i);

        // Read the band data
        if ( eType == GDT_UInt16  &&  cast_to_signed ) {
           // Cast the unsigned short int data to signed short int
           eErr = band->RasterIO(GF_Read, 0, 0, nXSize, nYSize, (void *)data, nXSize, nYSize, GDT_Int16, 0, 0);
        }
        else {
           // Read the data as is
           eErr = band->RasterIO(GF_Read, 0, 0, nXSize, nYSize, (void *)data, nXSize, nYSize, eType, 0, 0);
        }

        if ( eErr == CE_Failure ) {
           fprintf(stderr, "Error reading band %d from GDAL dataset - bye!\n", i);
           exit(1);
        }

        #if PRINT_DEBUG_INFO
            // Output the raster in ASCII format
            fprintf(stderr, "Band: %d\n", i);
            //dump_raster((void*)data, &hdr, 10, 10);
        #endif
    
        if ( DRRwrRaster(drrfile, &hdr, data, nXSize) != 0 ) {
           fprintf(stderr, "Error writing band %d to drr raster - bye!\n", i);
           exit(1);
        }
    } // Loop over bands
 
    DRRclose(drrfile);

    delete [] data;

    /* -------------------------------------------------------------------- */
    /*      Re-open dataset, and copy any auxilary pam information.         */
    /* -------------------------------------------------------------------- */
    DRRDataset *poDS = (DRRDataset *) GDALOpen( pszFilename, GA_ReadOnly );
    if( poDS )
        poDS->CloneInfo( poSrcDS, GCIF_PAM_DEFAULT );

    return poDS;
}

/************************************************************************/
/*                          GetGeoTransform()                           */
/************************************************************************/

CPLErr DRRDataset::GetGeoTransform( double * padfTransform )

{
    #if PRINT_DEBUG_INFO
        fprintf(stderr, "In DRRDataset::GetGeoTransform\n");
    #endif

    // Determine the type of geotransform
    if ( hdr.projection == 0 ) {
       #if PRINT_DEBUG_INFO
           fprintf(stderr, "Raster boundary LL: (%f, %f) -> UR: (%f, %f)\n",
           hdr.left, hdr.bottom, hdr.right, hdr.top);
       #endif

       // GDAL uses the top left corner of the pixel as the origin. DRR uses the pixel centre
       double pixel_width = ((double)hdr.right - (double)hdr.left)   / (double)(hdr.ncols - 1);
       double pixel_height= ((double)hdr.top   - (double)hdr.bottom) / (double)(hdr.nrows - 1);

       double dfLLLat  = (double)hdr.bottom + pixel_height/2.0;
       double dfLLLong = (double)hdr.left   - pixel_width /2.0;
       double dfURLat  = (double)hdr.top    + pixel_height/2.0;
       double dfURLong = (double)hdr.right  - pixel_width /2.0;

       // padfTransform[0]   x-coord of upper left corner of the upper left pixel 
       // padfTransform[1]   Pixel width
       // padfTransform[2]   
       // padfTransform[3]   y-coord of upper left corner of the upper left pixel 
       // padfTransform[4]   
       // padfTransform[5]   Pixel height

       padfTransform[0] = dfLLLong;
       padfTransform[3] = dfURLat;
       padfTransform[1] =      (dfURLong - dfLLLong) / (double)(GetRasterXSize() - 1);
       padfTransform[2] = 0.0;
        
       padfTransform[4] = 0.0;
       padfTransform[5] = -1 * (dfURLat - dfLLLat)   / (double)(GetRasterYSize() - 1);

       #if PRINT_DEBUG_INFO
           fprintf(stderr, "Raster boundary in GDAL format LL: (%f, %f) -> UR: (%f, %f)\n",
           dfLLLong, dfLLLat, dfURLong, dfURLat);
           fprintf(stderr, "Raster size: %d x %d Transform Element 1: %f, Transform Element 5: %f\n",
           GetRasterXSize(), GetRasterYSize(), padfTransform[1], padfTransform[5]);
       #endif
    }
    else {
       fprintf(stderr, "GeoTransform for Lambert Conformal Conic projection is not implemented yet\n");
       return CE_Failure;
    }

    return CE_None;
}

/************************************************************************/
/*                          GetProjectionRef()                          */
/************************************************************************/
/* copy in lat/long specification in WKT format */
/* this came from looking at a lat/long .img file... */
char szWG84_WKT[] = "GEOGCS[\"WGS_1984\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.2572235630016],TOWGS84[0,0,0,0,0,0,0]],PRIMEM[\"Greenwich\",0],UNIT[\"degree\",0.0174532925199433],AUTHORITY[\"EPSG\",\"4326\"]]";

const char *DRRDataset::_GetProjectionRef()
{
    #if PRINT_DEBUG_INFO
        fprintf(stderr, "In DRRDataset::GetProjectionRef\n");
    #endif

    // Determine the type of geotransform
    if ( hdr.projection == 0 ) {
        return szWG84_WKT;
    }
    else {
       fprintf(stderr, "GetProjectionRef for Lambert Conformal Conic projection is not implemented yet\n");
       return NULL;
    }

}

/************************************************************************/
/*                          GetSpatialRef()                             */
/************************************************************************/

const OGRSpatialReference *DRRDataset::GetSpatialRef() const

{
    return &m_oSRS;
}




/************************************************************************/
/*                          GDALRegister_DRR()                          */
/************************************************************************/

CPL_C_START
void GDALRegister_DRR(void);
CPL_C_END

void GDALRegister_DRR()

{
    #if PRINT_DEBUG_INFO
        fprintf(stderr, "In GDALRegister_DRR\n");
    #endif

    GDALDriver	*poDriver;

    if (! GDAL_CHECK_VERSION("DRR"))
        return;

    if( GDALGetDriverByName( "DRR" ) == NULL )
    {
        poDriver = new GDALDriver();
        
        poDriver->SetDescription( "DRR" );

        poDriver->SetMetadataItem( GDAL_DMD_LONGNAME, 
                                   "Qld. Dept. of Natural Resources, Mines and Water DRR (.drr)" );
        poDriver->SetMetadataItem( GDAL_DMD_HELPTOPIC, 
                                   "frmt_various.html#DRR" );
        poDriver->SetMetadataItem( GDAL_DMD_EXTENSION, "drr" );

        poDriver->pfnOpen       = DRRDataset::Open;
        poDriver->pfnCreateCopy = DRRCreateCopy;

        GetGDALDriverManager()->RegisterDriver( poDriver );
    }
}
